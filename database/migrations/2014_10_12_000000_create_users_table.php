<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 100);
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password', 200);
            $table->string('phone_number', 15);
            $table->string('image', 255);
            $table->boolean('admin')->default(0);
            // $table->string('old_password', 50);
            //            $table->timestamp('email_verified_at')->nullable();
            // $table->string('password', 100);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
