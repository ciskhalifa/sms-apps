<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsCampaignDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_campaign_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('user_id');
            $table->integer('groups_id');
            $table->integer('contacts_id');
            $table->integer('templates_id');
            $table->integer('maskings_id');
            $table->text('message');
            $table->string('phone_number', 50);
            $table->text('description');
            $table->boolean('is_sent');
            $table->timestamp('sent_at');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_campaign_details');
    }
}
