<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiletosmsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filetosms_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('filetosms_id');
            $table->integer('user_id');
            $table->integer('contacts_id');
            $table->integer('maskings_id');
            $table->text('message');
            $table->string('phone_number', 50);
            $table->text('description');
            $table->date('scheduler_date');
            $table->boolean('is_sent');
            $table->timestamp('sent_at');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filetosms_details');
    }
}
