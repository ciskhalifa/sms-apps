<!DOCTYPE html>
<html>
<head>
    @include('include.head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
</head>
    <title>{{ config('app.name', 'Website-Name') }}</title>
<body>
<div id="top-bg"></div>
<div id="nav-block"></div>
    @include('include.header')

    <!-- Main Wrapper -->
<div class="main-wrapper">
    <!-- Main Content -->
    <section>
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section><!-- Main Content /-->
</div><!-- Main Wrapper /-->
    @include('include.footer')
    @include('sweet::alert')
