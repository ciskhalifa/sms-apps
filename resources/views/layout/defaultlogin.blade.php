<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('include.head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<title>{{ config('app.name', 'Website-Name') }}</title>
<body>
<div id="top-bg"></div>
<div id="nav-block"></div>
<!-- Main Wrapper -->
<div class="main-wrapper">
    <!-- Main Content -->
    <section>
        <div class="container">
            @yield('content')
        </div>
    </section><!-- Main Content /-->
</div><!-- Main Wrapper /-->
@include('include.footer')
