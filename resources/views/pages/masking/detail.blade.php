@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Detail Masking Management</h1>
            <div class="breakv d-lg-none"></div>
            <div class="link-btn link-btn__primary link-btn--round link-btn--medium link-btn--padding-big shadow">{{$rowdata->status}}</div>
        </div>

        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form class="vali" action="masking-management.html">
                        <div class="form-team">
                            <label for="fjm">Jenis Masking</label>
                            <div class="form-team--arrow">
                                <select id="fjm" disabled>
                                    <option value="{{$rowdata->id}}" selected="" disabled>{{$rowdata->name}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-team">
                            <label for="fnamemas">Nama Masking</label>
                            <input type="text" id="fnamemas" name="phonenumber" placeholder="Masukan Nama Masking" value="{{$rowdata->name}}" disabled>
                        </div>
                        <div class="form-team">
                            <label for="description">Deskripsi</label>
                            <textarea id="description" placeholder="Tulis deskripsi atau catatan" disabled>{{$rowdata->description}}</textarea>
                        </div>
                        <div class="form-team">
                            <label>Dokumen Pengajuan</label>
                            <div class="form-file">
                                <input type="file" id="uploadDoc" class="hide" placeholder="Tidak ada yang dipilih" disabled>
                                <label for="uploadDoc" class="link-btn link-btn--small link-btn--sort link-btn__tertiary shadow">Pilih File</label>
                                <span class="file-label">{{$rowdata->file}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('maskingmanagement')}}" class="link-btn link-btn__negative">Kembali</a>
                            </div>
                            <div class="col-6">

                                <!-- Popup -->
                                <div class="modal fade" id="popupSubmit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Ajukan Masking?</div>
                                                    <span class="block">
                                                        Pengajuan masking akan kami proses paling lambat 1 bulan dari hari pengajuan.
                                                    </span>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Ajukan">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
@endsection
