@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Masking Management</h1>
            <div class="breakv d-lg-none"></div>
            <a href="{{route('masking.download')}}" class="link-btn link-btn__secondary link-btn--medium link-btn--padding shadow"><i class="fa fa-download"></i> Dokumen Pengajuan</a>
        </div>

        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form class="vali" action="{{route('masking.store')}}" enctype="multipart/form-data" method="POST">
                    {{csrf_field()}}
                        <div class="form-team">
                            <label for="fjm">Jenis Masking</label>
                            <div class="form-team--arrow">
                                <select id="fjm" name="type_id">
                                <option value="">- Pilihan -</option>
                                    @foreach ($rowdata as $row)
                                        <option value="{{$row->id}}">{{$row->type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-team">
                            <label for="fnamemas">Nama Masking</label>
                            <input type="text" id="fnamemas" placeholder="Masukan Nama Masking" name="name">
                        </div>
                        <div class="form-team">
                            <label for="description">Deskripsi</label>
                            <textarea name="description" id="description" placeholder="Tulis deskripsi atau catatan"></textarea>
                        </div>
                        <div class="form-team">
                            <label>Dokumen Pengajuan</label>
                            <div class="form-file">
                                <input name="file" type="file" id="uploadDoc" class="hide" placeholder="Tidak ada yang dipilih" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                <label for="uploadDoc" class="link-btn link-btn--small link-btn--sort link-btn__tertiary shadow">Pilih File</label>
                                <span class="file-label">Tidak ada yang dipilih</span>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('maskingmanagement')}}" class="link-btn link-btn__negative">Kembali</a>
                            </div>
                            <div class="col-6">
                                <button type="button" class="link-btn link-btn__primary shadow" data-toggle="modal" data-target="#popupSubmit">
                                    Ajukan
                                </button>
                                <!-- Popup -->
                                <div class="modal fade" id="popupSubmit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Ajukan Masking?</div>
                                                    <span class="block">
                                                        Pengajuan masking akan kami proses paling lambat 1 bulan dari hari pengajuan.
                                                    </span>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Ajukan">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
@endsection
