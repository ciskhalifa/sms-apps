@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Masking Management</h1>
            <div class="breakv d-lg-none"></div>
            <a href="{{route('masking.create')}}" class="link-btn link-btn__primary link-btn--medium link-btn--padding-big shadow">Ajukan Masking</a>
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-md-5 col-12" style="z-index:1">
                    <a href="{{route('masking.download')}}" class="link-btn link-btn__secondary link-btn--medium shadow" target="_BLANK"><i class="fa fa-download"></i> Dokumen Pengajuan</a>
                </div>
                <div class="col-12">
                    <table class="dtr table-team dt-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Masking</th>
                                <th>Jenis Masking</th>
                                <th>Status</th>
                                <th class="center-align">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($masking as $row)
                            <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->type->type_name }}</td>
                            <?php
                                $stat = '';
                                if ($row->status == "Pengajuan") {
                                    $stat .= "<span class='status status--progress'>" . $row->status . "</span>";
                                } elseif ($row->status == "Aktif") {
                                    $stat .= "<span class='status status--success'>" . $row->status . "</span>";
                                } elseif ($row->status == "Nonaktif") {
                                    $stat .= "<span class='status status--warning'>" . $row->status . "</span>";
                                } else {
                                    $stat .= "<span class='status status--decline'>" . $row->status . "</span>";
                                }
                                ?>
                            <td><?=$stat?></td>
                            <td class="center-align reset-p--hor">
                                <a href="maskingmanagement/{{$row->id}}/detail" class="breakh__right--medium">
                                <i class="fsize-p-6 fcolor-primary fa fa-eye"></i>
                                </a>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-12">
                    <table class="dtr-no-filter table-team dt-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama Masking</th>
                                <th>User</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($masking as $row)
                            <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->user->full_name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
<!-- Popup Delete -->
<div class="modal fade" id="popupDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <center>
                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Template?</div>
                </center>
                <div class="row breakh__top--medium-med">
                    <div class="col-6">
                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                    </div>
                    <div class="col-6">
                        <a href="address-book.html" class="link-btn link-btn__error shadow">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Popup Delete /-->

@endsection
