@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8" id="divcontent">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Tambah Template Pesan</h1>
            
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{ route('template.store') }}" method="post">
                        <input type = "hidden" name = "_token" value = "{{ csrf_token() }}">                    

                        <div class="form-team">
                            <label for="fname">Nama Template Pesan</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama Template Pesan" value="">
                        </div>
                        <div class="form-team">
                            <label for="fmessage">Pesan SMS</label>
                            <textarea name="content" id="fmessage" placeholder="Masukan Pesan"></textarea>
                        </div>
                        <div class="form-team">
                            <label for="fdes">Deskripsi</label>
                            <textarea name="description" id="fdes" placeholder="Masukan Deskripsi"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <a href="{{ url ('template') }}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
</div>      
@endsection
