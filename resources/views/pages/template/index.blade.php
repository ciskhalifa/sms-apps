@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8" id="divcontent">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Template Pesan</h1>
            <div class="breakv d-lg-none"></div>
            <a href="{{url('template/create')}}" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow"><i class="fa fa-plus"></i> Tambah Template</a>
        </div>
        
        <div class="tab-content wrapper--main-body wrapper--padding--large-equal">
            <div class="flexed">
                <input type="hidden" class="judul" value="Template">
                <span class="block lblpilih">0 Template dipilih</span>
                <a href="#!" class="btnDelAll block breakh__left--medium" data-toggle="modal" data-target="#popupDelete" style="z-index:1; display: block;" ><i class="icon--medium fa fa-trash"></i>&nbsp;&nbsp; Hapus Template</a>
            </div>
            <table class="dtr table-team dt-responsive tblTemplate" style="width:100%">
                <thead>
                    <tr>
                        <th>
                            <label class="checkbox-wrapper">
                                <input name="selectall" data-id="all" type="checkbox" class="master">
                                <span class="checkmark"></span>
                            </label>
                        </th>
                        <th>Template Pesan</th>
                        <th>Deskripsi Template</th>
                        <th class="center-align reset-p--hor">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowdata as $row)
                    <tr>
                        <td>
                            <label class="checkbox-wrapper">
                                <input type="checkbox" data-id="{{$row->id}}" value="{{$row->id}}" class="sub_chk"/>
                                <span class="checkmark"></span>
                            </label>
                        </td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->description }}</td>
                        <td class="center-align reset-p--hor">
                            <a href="template/{{$row->id}}/edit" class="breakh__right--medium">
                                <i class="fsize-p-6 fcolor-secondary fa fa-edit"></i>
                            </a>
                            <a href="#!" class="btnDel" data-toggle="modal" data-target="#popupDelete" data-default="{{$row->id}}">
                                <i class="fsize-p-6 fcolor-tertiary fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Popup Delete -->   
<div class="modal fade" id="popupDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="{{ url('template/destroy')}}" method="post">
                    @method('DELETE')
                    {{csrf_field()}}
                    <input type="hidden" id="cid" name="cid">
                    <center>
                        <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Template?</div>
                    </center>
                    <div class="row breakh__top--medium-med">
                        <div class="col-6">
                            <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="link-btn link-btn__error shadow" class="hapus">Hapus</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- Popup Delete /-->
@endsection

