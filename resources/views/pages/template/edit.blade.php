@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8" id="divcontent">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Edit Template Pesan</h1>
            <div class="breakv d-lg-none"></div>
            <i class="fa fa-trash fcolor-tertiary breakh__top--x-small fsize-p-14 c-pointer" data-toggle="modal" data-target="#popupDeleteContact"></i>
            <!-- Popup Contact Delete -->
            <div class="modal fade" id="popupDeleteContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form action="{{ route('template.destroy' , $data->id) }}" method="post">
                                @method('DELETE')
                                {{csrf_field()}}
                                <input type="hidden" id="cid" name="cid" value="{{ $data->id }}">
                                <center>
                                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Template?</div>
                                </center>
                                <div class="row breakh__top--medium-med">
                                    <div class="col-6">
                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                    </div>
                                    <div class="col-6">
                                    <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- Popup Contact Delete /-->
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{ route('template.update', $data->id) }}" method="post">
                        @method('PATCH')
                        <input type = "hidden" name = "_token" value = "{{ csrf_token() }}">
                        <input type = "hidden" name = "id" value = "{{ $data->id }}">
                        <div class="form-team">
                            <label for="fname">Nama Template Pesan</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama Template Pesan" value="{{ $data->name }}">
                        </div>
                        <div class="form-team">
                            <label for="fmessage">Pesan SMS</label>
                            <textarea name="content" id="fmessage" placeholder="Masukan Pesan">{{ $data->content }}</textarea>
                        </div>
                        <div class="form-team">
                            <label for="fdes">Deskripsi</label>
                            <textarea name="description" id="fdes" placeholder="Masukan Deskripsi">{{ $data->description }}</textarea>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <a href="{{ url ('template') }}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
</div>
@endsection
