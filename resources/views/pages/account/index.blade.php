@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <h1 class="fsize-p-18 fbold wrapper--padding--large">Account</h1>
        <!-- Tab -->
        <div class="tab-team">
            <div class="tab-team__head">
                <ul class="nav tab-team__head__link-wrapper">
                    <li class="tab-team__head__link-wrapper__link">
                        <a class="active" data-toggle="pill" href="#info">Informasi</a>
                    </li>
                    <li class="tab-team__head__link-wrapper__link">
                        <a class="" data-toggle="pill" href="#pass">Kata Sandi</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content wrapper--main-body wrapper--padding--large-equal">
                <div class="tab-pane active" id="info">
                    <div class="user-avatar flexed form-team form-team__img">
                        <img id="preview" src="{{(isset($rowdata) && $rowdata->image != '' ? 'uploads/'.$rowdata->image : '/assets/img/user.jpg')}}" alt="Avatar" id="uploadImg" class="avatar avatar--large">
                        <div>
                            <form method="post" action="{{ route('account.changeimage') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <label class="fbold">Tambah Foto</label>
                                <span class="fsize-m-2 fcolor-disabled block">JPG, GIF or PNG. Maks size 800K</span>
                                <div class="breakv--small"></div>
                                <div class="flexed">
                                    <div class="breakh__right--small">
                                        <label for="upload-img-btn" class="link-btn link-btn--small link-btn--sort link-btn__tertiary shadow reset-m">Pilih File</label>
                                        <input type="file" name="image" id="upload-img-btn" class="hide" placeholder="Tidak ada yang dipilih">
                                        <div class="breakv"></div>
                                        <button type="submit" value="Ganti Foto" class="link-btn link-btn--small link-btn--sort link-btn__secondary shadow">Ganti Foto</button>
                                    </div>
                                    <div class="tabled">
                                        <span class="tabled tabled__cell tabled__cell--middle fsize-m-2 fcolor-disabled file-label" style="height:3.6rem">Tidak ada yang dipilih</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <form class="vali" action="{{route('account.updatedata')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-team">
                                    <label for="ffullname">Nama</label>
                                    <input type="text" id="ffullname" name="full_name" placeholder="Masukan Nama" value="{{ $rowdata->full_name }}">
                                </div>
                                <div class="form-team">
                                    <label for="femail">Email</label>
                                    <input type="email" id="femail" name="email" placeholder="Masukan Email" value="{{ $rowdata->email }}">
                                </div>
                                <div class="form-team">
                                    <label for="ftelp">Nomor Telepon</label>
                                    <input type="number" id="ftelp" name="phone_number" placeholder="Masukan Nomor Telepon" value="{{ $rowdata->phone_number }}">
                                </div>
                                <div class="row">
                                    <div class="col-6 offset-6">
                                        <input type="submit" value="Simpan" class="link-btn link-btn__primary shadow">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="pass">
                    <span>Anda dapat mengubah kata sandi akun dibawah ini</span>
                    <div class="breakv"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <form id="newPass" action="{{route('account.changepassword')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-team">
                                    <label for="fpassword">Kata Sandi Lama</label>
                                    <input type="password" id="fpassword" name="old_password" placeholder="Masukan Kata Sandi Lama">
                                </div>
                                <div class="form-team">
                                    <label for="new_password">Kata Sandi Baru</label>
                                    <input type="password" id="new_password" name="new_password" placeholder="Masukan Kata Sandi Baru">
                                </div>
                                <div class="form-team">
                                    <label for="fconfirm_password">Ulangi Kata Sandi Baru</label>
                                    <input type="password" id="rfconfirm_password" name="confirm_password" placeholder="Ulangi Kata Sandi Baru">
                                </div>
                                <div class="row">
                                    <div class="col-6 offset-6">
                                        <input type="submit" value="Simpan" class="link-btn link-btn__primary shadow">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
@endsection
