@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Buat Grup</h1>
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <form action="{{route('group.create')}}" method="POST">
                <input type = "hidden" name = "_token" value = "{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-team">
                            <label for="fname">Nama Grup</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama Grup">
                        </div>
                        <div class="form-team">
                            <label for="fdes">Deskripsi</label>
                            <textarea id="fdes" name="description" placeholder="Tulis Deskripsi"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row breakh__bottom--medium-med">
                            <div class="col-sm-6">
                                <div class="form-team">
                                    <label for="fdes">Jumlah Kontak</label>
                                    <input type="hidden" class="judul" value="Kontak">
                                    <input type="hidden" class="contacts_number" name="contacts_number[]">
                                    <input type="hidden" class="contacts_name" name="contacts_name[]">
                                    <span class="fcolor-primary block breakh__top--small lblpilih">0 Kontak dipilih</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="flexed">
                                    <div class="flexed__1-w"></div>
                                    <a href="#" data-toggle="modal" data-target="#popupImport" class="link-btn link-btn__secondary link-btn--medium link-btn--padding shadow breakh__bottom--medium import"><i class="fa fa-upload"></i> Import Kontak</a>
                                </div>
                            </div>
                        </div>
                        <table class="table-team dt-responsive tblkontak" style="width:100%" >
                            <thead>
                                <tr>
                                    <th>
                                        <label class="checkbox-wrapper">
                                            <input name="selectall" data-id="all" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <th>Nama</th>
                                    <th>Nomor Telepon</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="col-6 offset-6">
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('address')}}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Buat Grup">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- Tab / -->
    </div>
</div>
<!-- Popup Contact Import -->
<div class="modal fade" id="popupImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form id="xfrm" name="ximport" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <label for="upload-img-btn" class="link-btn link-btn--small link-btn__tertiary shadow reset-m">Pilih File</label>
                    <input type="file" name="file" id="upload-img-btn" class="hide" placeholder="Tidak ada yang dipilih" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <div class="tabled">
                        <span class="tabled tabled__cell tabled__cell--middle fsize-m-2 fcolor-disabled file-label" style="height:3.6rem">Tidak ada yang dipilih</span>
                    </div>
                    <div class="row breakh__top--medium-med">
                        <div class="col-6">
                            <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                        </div>
                        <div class="col-6">
                            <button type="button" id="ximport" class="link-btn link-btn__secondary shadow">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- Popup Contact Import /-->
@endsection
