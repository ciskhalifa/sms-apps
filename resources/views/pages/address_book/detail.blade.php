@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Detail Kontak</h1>
            <!-- <div class="breakv d-lg-none"></div>
            <a href="topup-balance-input.html" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow"><i class="fa fa-plus"></i> Tambah Kontak</a> -->
        </div>

        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <div class="fcolor-default2">
                        <div class="form-team row">
                            <div class="col-12 col-md-6">
                                <span class="fbold">Nama :</span>
                            </div>
                            <div class="col-12 col-md-6">
                                <span class="block">{{$contact->name}}</span>
                            </div>
                        </div>
                        <div class="form-team row">
                            <div class="col-12 col-md-6">
                                <span class="fbold">Nomor Telepon :</span>
                            </div>
                            <div class="col-12 col-md-6">
                                <span class="block">{{$contact->phone_number}}</span>
                            </div>
                        </div>
                        <div class="form-team row">
                            <div class="col-12 col-md-6">
                                <span class="fbold">Grup :</span>
                            </div>
                            <div class="col-12 col-md-6">
                                <?php $i = 1; ?>
                                @foreach($contact->group as $row)
                                <span class="block breakh__bottom--small"><?= $i ?>. {{$row->name}}</span>
                                <?php $i++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <a href="{{url('address')}}" class="link-btn link-btn__negative">Kembali</a>
                        </div>
                        <div class="col-6">
                            <a href="{{url('quicksms')}}" class="link-btn link-btn__primary shadow">Kirim SMS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
@endsection