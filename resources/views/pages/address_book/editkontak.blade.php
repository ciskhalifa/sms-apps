@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Ubah Kontak</h1>
            <div class="breakv d-lg-none"></div>
            <i class="fa fa-trash fcolor-tertiary breakh__top--x-small fsize-p-14 c-pointer" data-toggle="modal" data-target="#popupDeleteContact"></i>
            <!-- Popup Contact Delete -->
            <div class="modal fade" id="popupDeleteContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form action="{{ route('contact.destroy') }}" method="post">
                                @method('DELETE')
                                {{csrf_field()}}
                                <input type="hidden" id="cid" name="cid" value="{{ $contact->id }}">
                                <center>
                                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Kontak?</div>
                                </center>
                                <div class="row breakh__top--medium-med">
                                    <div class="col-6">
                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                    </div>
                                    <div class="col-6">
                                        <button type="sumbit" class="link-btn link-btn__error shadow">Hapus</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- Popup Contact Delete /-->
        </div>

        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{ route('update.contact', $contact->id) }}" method="POST">
                        @method('PATCH')
                        <input type = "hidden" name = "_token" value = "{{ csrf_token() }}">
                        <input type = "hidden" id = "cid" value = "{{ $contact->id }}">
                        <div class="form-team">
                            <label for="fname">Nama</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama" value="{{$contact->name}}">
                        </div>
                        <div class="form-team">
                            <label for="fphonenumber">Nomor Telepon</label>
                            <input type="number" id="fphonenumber" name="phone_number" placeholder="Masukan Nomor Telepon" value="{{$contact->phone_number}}">
                        </div>
                        <div class="form-team">
                            <label for="client">Grup</label>
                            <?php $no = 0;?>
                            @foreach ($contact->group as $key => $rowgroup)
                            <div class="form-team--arrow breakh__bottom" >
                                <select name="groups_id[]" id="selectgroup<?php echo ($no + 1) ?>"  class="pilihangroup">
                                    <option value="{{$rowgroup->id}}" @if($rowgroup->id == $contact->group[$key]->id) selected @endif>{{$rowgroup->name}}</option>
                                </select>
                            </div>
                            <?php $no++;?>
                            @endforeach
                            <div class="input_fields_wrap breakh__bottom--big">
                                <div class="add-input-btn add_field_button fcolor-primary fbold fsize-p-4 center-align">+ Tambah Grup</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('address')}}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
</div>
@endsection
