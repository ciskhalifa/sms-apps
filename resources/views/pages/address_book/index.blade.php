@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Address Book</h1>
            <div class="breakv d-lg-none"></div>
            <a href=#! data-toggle="modal" data-target="#popupImport" class="fsize-p-6 breakh__top--x-small breakh__bottom--x-small breakh__right fcolor-quaternary import"><i class="fa fa-upload"></i></a>
            <a href="{{route('export.contact')}}" class="fsize-p-6 breakh__top--x-small breakh__bottom--x-small breakh__right fcolor-secondary export"><i class="fa fa-download"></i></a>

            <a href="{{url('address/kontak')}}" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow add-c _active"><i class="fa fa-plus"></i> Tambah Kontak</a>

            <a href="{{url('address/group')}}" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow add-g"><i class="fa fa-plus"></i> Tambah Grup</a>
        </div>
        <!-- Tab -->
        <div class="tab-team">
            <div class="tab-team__head">
                <ul class="nav tab-team__head__link-wrapper">
                    <li class="tab-team__head__link-wrapper__link">
                        <a class="btn-tab-c active" data-toggle="pill" href="#contact">Kontak</a>
                    </li>
                    <li class="tab-team__head__link-wrapper__link">
                        <a class="btn-tab-g" data-toggle="pill" href="#group">Grup</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content wrapper--main-body wrapper--padding--large-equal">
                <div class="tab-pane active" id="contact">
                    <div class="row">
                        <div class="col-12 col-md-7" style="z-index:1">
                            <div class="flexed block-sm">
                                <input type="hidden" class="judul" value="Kontak">
                                <span class="breakh__right breakh__top--small breakh__bottom--small lblpilih">0 Kontak Dipilih</span>

                                <div class="link-btn-expand-wrapper">
                                    <div class="link-btn link-btn__negative-primary link-btn--small link-btn--padding link-btn--expand link-btn--expand--arrow"><span class="breakh__right--big">Tambah ke Grup</span>
                                    </div>

                                    <div class="link-btn-expand-wrapper__content _v2 shadow">
                                        <!-- <div class="link-btn-expand-wrapper__content__block-btn link-btn-expand-wrapper__content__block-btn--medium"></div> -->
                                        <div class="link-btn-expand-wrapper__content__main">
                                            <div class="_top">
                                                <div class="form-team form-team--search-input form-team--search-input--small">
                                                    <input type="text" id="name" placeholder="Cari Grup" class="input-small">
                                                </div>
                                                @foreach ($group as $key => $row)
                                                <label class="checkbox-wrapper checkbox-wrapper--2">
                                                    <input type="checkbox" name="grups" value="{{$row->id}}" class="ceklisgroup">
                                                    <span class="checkmark"></span>
                                                    <div class="select-contact">
                                                        <span class="block">{{$row->name}}  <img class="fa-spin load-spin _load hide loading{{$row->id}}" src="{{url('assets/img/loading.png')}}"> </span> 
                                                    </div>
                                                </label>
                                                @endforeach
                                                
                                            </div>

                                            <div class="_bottom">
                                                <a href="{{url('address/group')}}">+ Buat Grup Baru</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <!-- Popup Contact Delete -->
                            <div class="modal fade" id="popupDeleteContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <form action="{{ route('contact.destroy') }}" method="post">
                                                @method('DELETE')
                                                {{csrf_field()}}
                                                <input type="hidden" id="cid" name="cid">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Kontak?</div>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Popup Contact Delete /-->

                            <table class="dtr table-team dt-responsive" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>
                                            <label class="checkbox-wrapper">
                                                <input name="selectall" data-id="all" type="checkbox" class="masterkontak">
                                                <span class="checkmark"></span>
                                            </label>
                                        </th>
                                        <th>Nama</th>
                                        <th>Nomor Telepon</th>
                                        <th class="center-align reset-p--hor">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($contact as $row)
                                    <tr>
                                        <td>
                                            <label class="checkbox-wrapper">
                                                <input type="checkbox" data-id="{{$row->id}}" value="{{$row->id}}" class="sub_chk_kontak"/>
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->phone_number }}</td>
                                        <td class="center-align reset-p--hor">
                                            <a href="address/{{$row->id}}/detail"  class="breakh__left--medium breakh__right--medium">
                                                <i class="fsize-p-6 fcolor-primary fa fa-eye"></i>
                                            </a>
                                            <a href="address/{{$row->id}}/editkontak" class="breakh__right--medium">
                                                <i class="fsize-p-6 fcolor-secondary fa fa-edit"></i>
                                            </a>
                                            <a href="#!" class="btnDel" data-toggle="modal" data-target="#popupDeleteContact" data-default="{{$row->id}}">
                                                <i class="fsize-p-6 fcolor-tertiary fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="group">
                    <div class="row">
                        <div class="col-12 col-md-7" style="z-index:1">
                            <div class="flexed block-sm">
                                <input type="hidden" class="judul" value="Kontak">
                                <span class="breakh__right breakh__top--small breakh__bottom--small lblpilih">0 Kontak Dipilih</span>

                                <a href="#!" style="padding: .8rem 0" class="btnDelAllGrp breakh__left" data-toggle="modal" data-target="#popupDeleteGroup"><i class="fa fa-trash fsize-p-4 breakh__right--small" style="position: relative;top: .1rem;"></i> Hapus Grup</a>
                            </div>
                        </div>

                        <div class="col-12">
                            <!-- Popup Group Delete -->
                            <div class="modal fade" id="popupDeleteGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <form action="{{ route('group.destroy') }}" method="post">
                                                @method('DELETE')
                                                {{csrf_field()}}
                                                <input type="hidden" id="cidgrp" name="cid">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Grup?</div>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Popup Group Delete /-->
                            <table class="dtr table-team dt-responsive" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>
                                            <label class="checkbox-wrapper">
                                                <input name="selectall" data-id="all" type="checkbox" class="mastergroup">
                                                <span class="checkmark"></span>
                                            </label>
                                        </th>
                                        <th>Nama Grup</th>
                                        <th>Jumlah Member</th>
                                        <th class="center-align reset-p--hor">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($group as $row)
                                    <tr>
                                        <td>
                                            <label class="checkbox-wrapper">
                                                <input type="checkbox" data-id="{{$row->id}}" value="{{$row->id}}" class="sub_chk_group" />
                                                <span class="checkmark"></span>
                                            </label>
                                        </td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->contact->count() }}</td>
                                        <td class="center-align reset-p--hor">
                                            <a href="address/{{$row->id}}/editgroup" class="breakh__right--medium">
                                                <i class="fsize-p-6 fcolor-secondary fa fa-edit"></i>
                                            </a>
                                            <a href="#!" class="btnDelGrp" data-toggle="modal" data-target="#popupDeleteGroup" data-default="{{$row->id}}">
                                                <i class="fsize-p-6 fcolor-tertiary fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
<!-- Popup Contact Import -->
<div class="modal fade" id="popupImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form name="import" action="{{ route('import.contact') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <label for="upload-img-btn" class="link-btn link-btn--small link-btn__tertiary shadow reset-m">Pilih File</label>
                    <input type="file" name="file" id="upload-img-btn" class="hide" placeholder="Tidak ada yang dipilih" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <div class="tabled">
                        <span class="tabled tabled__cell tabled__cell--middle fsize-m-2 fcolor-disabled file-label" style="height:3.6rem">Tidak ada yang dipilih</span>
                    </div>
                    <div class="row breakh__top--medium-med">
                        <div class="col-6">
                            <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="link-btn link-btn__secondary shadow">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- Popup Contact Import /-->

@endsection
