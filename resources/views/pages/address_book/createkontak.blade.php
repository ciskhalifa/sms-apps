@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Tambah Kontak</h1>
        </div>

        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{route('contact.create')}}" method="POST">
                        <input type = "hidden" name = "_token" value = "{{ csrf_token() }}"> 
                        <input type = "hidden" id = "cid" value = "">
                        <div class="form-team">
                            <label for="fname">Nama</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama" value="">
                        </div>
                        <div class="form-team">
                            <label for="fphonenumber">Nomor Telepon</label>
                            <input type="number" id="fphonenumber" name="phone_number" placeholder="Masukan Nomor Telepon" value="">
                        </div>
                        <div class="form-team">
                            <label for="client">Grup</label>
                            <div class="form-team--arrow breakh__bottom" >
                                <select name="groups_id[]" id="selectgroup1" class="pilihangroup"></select>
                            </div>
                            <div class="input_fields_wrap breakh__bottom--big">
                                <div class="add-input-btn add_field_button fcolor-primary fbold fsize-p-4 center-align">+ Tambah Grup</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('address')}}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
</div>
@endsection
