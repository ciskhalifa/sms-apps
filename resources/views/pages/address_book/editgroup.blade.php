@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Ubah Grup</h1>
            <div class="breakv d-lg-none"></div>
            <i class="fa fa-trash fcolor-tertiary breakh__top--x-small fsize-p-14 c-pointer" data-toggle="modal" data-target="#popupDeleteGroup"></i>
            <!-- Popup Contact Delete -->
            <div class="modal fade" id="popupDeleteGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form action="{{ route('group.destroy') }}" method="post">
                                @method('DELETE')
                                {{csrf_field()}}
                                <input type="hidden" id="cid" name="cid" value="{{$group->id}}">
                                <center>
                                    <div class="fsize-p-4 fbold breakh__bottom--small">Hapus Grup?</div>
                                </center>
                                <div class="row breakh__top--medium-med">
                                    <div class="col-6">
                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                    </div>
                                    <div class="col-6">
                                        <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- Popup Contact Delete /-->
        </div>
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <form action="{{ route('update.group', $group->id) }}" method="POST">
                <input type = "hidden" name = "_token" value = "{{ csrf_token() }}">
                @method('PATCH')
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-team">
                            <label for="fname">Nama Grup</label>
                            <input type="text" id="fname" name="name" placeholder="Masukan Nama Grup" value="{{$group->name}}">
                        </div>
                        <div class="form-team">
                            <label for="fdes">Deskripsi</label>
                            <textarea id="fdes" name="description" placeholder="Tulis Deskripsi">{{$group->description}}</textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row breakh__bottom--medium-med">
                            <div class="col-sm-6">
                                <div class="form-team">
                                    <label for="fdes">Jumlah Kontak</label>
                                    <input type="hidden" class="judul" value="Kontak">

                                </div>
                            </div>

                        </div>
                        <table class="dtr table-team dt-responsive" style="width:100%" id="myTable">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Nomor Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($group->contact as $row)
                                <tr>

                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->phone_number }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="col-6 offset-6">
                        <div class="row">
                            <div class="col-6">
                                <a href="{{url('address')}}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary shadow" value="Update Grup">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- Tab / -->
    </div>
</div>
@endsection
