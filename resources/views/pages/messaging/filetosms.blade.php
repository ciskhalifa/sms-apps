@extends('layout.default')
@section('content')
@include('include.sidebar')

<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">File To SMS</h1>
        </div>

        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="tab-team2">
                <div class="tab-team2__head">
                    <ul class="nav tab-team2__head__link-wrapper">
                        <li class="tab-team2__head__link-wrapper__link">
                            <a class="active" href="{{route('filetosms.create')}}" >Upload File</a>
                        </li>
                        <li class="tab-team2__head__link-wrapper__link">
                            <a class="fcolor-primary" href="{{route('sample.download')}}">Download Sample</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-pane active">
                    <!-- Popup Delete -->
                    <div class="modal fade" id="popupDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                <form action="{{ route('filetosms.destroy') }}" method="post">
                                @method('DELETE')
                                {{csrf_field()}}
                                <input type="hidden" id="cidfiletosms" name="cid">
                                    <center>
                                        <div class="fsize-p-4 fbold breakh__bottom--small">Hapus File To SMS?</div>
                                    </center>
                                    <div class="row breakh__top--medium-med">
                                        <div class="col-6">
                                            <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                        </div>
                                        <div class="col-6">
                                        <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!-- Popup Delete /-->
                    <table class="dtr table-team dt-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Campaign Name</th>
                                <th>Masking</th>
                                <th>Total SMS</th>
                                <th class="center-align reset-p--hor">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($filetosms as $row)
                            <tr>
                                <td>{{$row->campaign_name}}</td>
                                <td>{{$row->campaign_name}}</td>
                                <td>{{$row->campaign->count()}}</td>
                                <td class="center-align reset-p--hor">
                                    <a href="filetosms/{{$row->id}}/detail">
                                        <i class="fsize-p-6 fcolor-primary fa fa-eye breakh__right--medium"></i>
                                    </a>
                                    <a href="#!" class="btnDelFiletoSmsindex" data-toggle="modal" data-target="#popupDelete" data-default="{{$row->id}}">
                                        <i class="fsize-p-6 fcolor-tertiary fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
