@extends('layout.default')
@section('content')
@include('include.sidebar')
<div id="divcontent"  class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Quick SMS</h1>
        </div>

        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{route('quick.store')}}" method="POST">
                    {{csrf_field()}}
                        <div class="form-team">
                            <label for="masking">Masking</label>
                            <div class="form-team--arrow">
                                <select id="masking" name="masking_id" class="maskingselect">
                                    <option>- Pilihan -</option>
                                    @foreach ($masking as $rowmasking)
                                    <option value="{{$rowmasking->id}}" data-user="{{$rowmasking->username}}" data-pass="{{$rowmasking->password}}">{{$rowmasking->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="username" id="smsuser">
                            <input type="hidden" name="password" id="smspass">
                        </div>
                        <div class="form-team">
                            <label for="topupValue">Campaign Name</label>
                            <input type="text" id="topupValue" placeholder="Masukan Nama Campaign" name="name" value="{{$kodecampaign}}" readonly>
                        </div>
                        <div class="form-team">
                            <label for="receiver">Penerima</label>
                            <label class="_right fbold fcolor-primary" id="penerima">0</label>
                            <input type="text" name='receiver[]' id='receiver' placeholder='Tambahkan Nomor Telepon'>
                        </div>
                        <div class="form-team">
                            <label for="template">Template Pesan</label>
                            <div class="form-team--arrow">
                                <select id="template" class="messages-template" name="template_id">
                                    <option value="">- Pilihan -</option>
                                    @foreach ($template as $rowtemplate)
                                    <option value="{{$rowtemplate->id}}" data-isi="{{$rowtemplate->content}}">{{$rowtemplate->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-team">
                            <label for="messages">Pesan</label>
                            <textarea id="messages" class="txtarea" placeholder="Tulis deskripsi atau catatan" name="messages"></textarea>
                            <div class="flexed fcolor-disabled">
                                <div class="flexed__1-w">SMS: <span id="cmessages">1</span></div>
                                <div>
                                    <span id="ctotal">0</span>/160
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="link-btn link-btn__negative btnPreview" data-toggle="modal" data-target="#popupPreview">
                                    Preview
                                </button>
                                <!-- Popup -->
                                <div class="modal fade" id="popupPreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Preview</div>
                                                    <span class="block">
                                                        Berikut adalah preview pesan yang akan dikirimkan
                                                    </span>
                                                    <div class="preview-message"></div>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Kirim">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                            <div class="col-6">
                                <button type="button" class="link-btn link-btn__primary shadow" data-toggle="modal" data-target="#popupSubmit">
                                    Kirim
                                </button>
                                <!-- Popup -->
                                <div class="modal fade" id="popupSubmit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Kirim Pesan?</div>
                                                    <span class="block">
                                                        Selalu periksa kembali pesan yang akan dikirimkan, jika sudah benar klik tombol kirim
                                                    </span>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Kirim">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
