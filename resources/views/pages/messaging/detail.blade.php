@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">File To SMS</h1>
            <div class="breakv d-lg-none"></div>
            <i class="fa fa-trash fcolor-tertiary breakh__top--x-small fsize-p-14 c-pointer" data-toggle="modal" data-target="#popupDelete" class="btnDelFiletoSmsDetail" ></i>
            <!-- Popup Delete -->
            <div class="modal fade" id="popupDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form action="{{ route('filetosms.destroy') }}" method="post">
                            @method('DELETE')
                            {{csrf_field()}}
                            <input type="hidden" id="cidfiletosms" name="cid" value="{{$filetosms[0]->filetosms_id}}">
                            <center>
                                <div class="fsize-p-4 fbold breakh__bottom--small">Hapus File To SMS?</div>
                            </center>
                            <div class="row breakh__top--medium-med">
                                <div class="col-6">
                                    <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                </div>
                                <div class="col-6">
                                <button type="submit" class="link-btn link-btn__error shadow">Hapus</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- Popup Delete /-->
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-team">
                        <label for="topupValue">Campaign Name</label>
                        <input type="text" id="topupValue" placeholder="Masukan Nama Campaign" name="campaign_name" value="{{$filetosms[0]->campaign->campaign_name}}" disabled>
                    </div>
                    <div class="form-team">
                        <label for="fmasking">Masking</label>
                        <div class="form-team--arrow">
                            <select id="masking" name="masking" class="maskingselect">
                                @foreach ($masking as $rowmasking)
                                <?php $a = ($rowmasking->id == $filetosms[0]->maskings_id) ? 'selected=selected' : 'disabled'; ?>
                                <option value="{{$rowmasking->id}}" data-user="{{$rowmasking->username}}" data-pass="{{$rowmasking->password}}" <?= $a ?>>{{$rowmasking->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <table class="dtr-no-filter table-team dt-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Penerima</th>
                                <th>Pesan</th>
                                <th>Tanggal Dikirim</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($filetosms as $row)
                                <tr>
                                    <td>{{$row->contact->phone_number}}</td>
                                    <td>{{$row->message}}</td>
                                    <td>{{$row->scheduler_date}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-6 offset-6">
                    <div class="row">
                        <div class="col-6 offset-6">
                            <a href="{{route('filetosms')}}" class="link-btn link-btn__negative">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Tab / -->
    </div>
</div>
@endsection
