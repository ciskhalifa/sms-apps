@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Inbox</h1>
            <div class="breakv d-lg-none"></div>
            <a href="#!" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow"><i class="fa fa-plus"></i> Mulai Percakapan</a>
        </div>

        <div class="wrapper--main-body">
            <div class="row row--no inbox">
                <div class="col-6 inbox__left">
                    <div class="form-team form-team--search-input">
                        <input type="text" id="name" placeholder="Cari Percakapan">
                    </div>
                    <div class="inbox__left__contacts">
                        <div class="inbox__left__contacts__empty">
                            <img src="/assets/img/illust-pesan.png">
                            <span>Belum ada percakapan atau pesan masuk saat ini.</span>
                            <a href="#!" class="link-btn link-btn__primary link-btn--medium link-btn--padding shadow"><i class="fa fa-plus"></i> Mulai Percakapan</a>
                        </div>
                        <div class="inbox__left__contacts__fill hide">
                            <a href="#!" class="inbox__left__contacts__fill__contact block">
                                <div class="_first flexed">
                                    <span class="_contact block flexed__1-w">Michelle Sudirman</span>
                                    <span class="_notice"></span>
                                </div>
                                <div class="_second flexed">
                                    <span class="_content block flexed__1-w">Lorem ipsum</span>
                                    <span class="_date">06/07/1960</span>
                                </div>
                            </a>

                            <a href="#!" class="inbox__left__contacts__fill__contact _newM block">
                                <div class="_first flexed">
                                    <span class="_contact block flexed__1-w">Audri Candra</span>
                                    <span class="_notice"></span>
                                </div>
                                <div class="_second flexed">
                                    <span class="_content block flexed__1-w">Dolor sit amet lorem ipsum</span>
                                    <span class="_date">07/07/1960</span>
                                </div>
                            </a>

                            <a href="#!" class="inbox__left__contacts__fill__contact block">
                                <div class="_first flexed">
                                    <span class="_contact block flexed__1-w">Nurdin</span>
                                    <span class="_notice"></span>
                                </div>
                                <div class="_second flexed">
                                    <span class="_content block flexed__1-w">Lorem ipsum</span>
                                    <span class="_date">06/07/1960</span>
                                </div>
                            </a>

                            <a href="#!" class="inbox__left__contacts__fill__contact _readM block">
                                <div class="_first flexed">
                                    <span class="_contact block flexed__1-w">Sana</span>
                                    <span class="_notice"></span>
                                </div>
                                <div class="_second flexed">
                                    <span class="_content block flexed__1-w">Lorem ipsum</span>
                                    <span class="_date">06/07/1960</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 inbox__right">
                    <div class="inbox__right__new hide">
                        <div class="inbox__right__new__head">
                            <a href="#!"><i class="fa fa-arrow-left"></i></a><span>Percakapan Baru</span>
                        </div>
                        <div class="inbox__right__new__to">
                            <label for="to">Kepada :</label><input id="to" type="search" placeholder="Ketik Nama atau Nomor Telepon">
                        </div>
                        <div class="inbox__right__new__contacts">
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Sana Nurhayati</span>
                                <span class="_number block">(62) 80987654321</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Michelle Sudirman</span>
                                <span class="_number block">(62) 80123456789</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Lalisa</span>
                                <span class="_number block">(62) 81029384756</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">David Kuncoro</span>
                                <span class="_number block">(62) 81234567890</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Budi McRoad</span>
                                <span class="_number block">(62) 80987654321</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Savira Al-Haq</span>
                                <span class="_number block">(62) 80123456789</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Timmy Tomson</span>
                                <span class="_number block">(62) 81029384756</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Ujang Abdullah</span>
                                <span class="_number block">(62) 81234567890</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Gerry Laksono</span>
                                <span class="_number block">(62) 80987654321</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Merry Moon</span>
                                <span class="_number block">(62) 80123456789</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Musa Tamimi</span>
                                <span class="_number block">(62) 81029384756</span>
                            </a>
                            <a href="#!" class="inbox__right__new__contacts__contact block">
                                <span class="_name block">Jennie Asraq</span>
                                <span class="_number block">(62) 81234567890</span>
                            </a>
                        </div>
                    </div>
                    <div class="inbox__right__conversation hide">
                        <div class="inbox__right__conversation__head flexed">
                            <div class="inbox__right__conversation__head__contact block flexed__1-w">
                                <span class="_name block">Sana Nurhayati</span>
                                <span class="_number block">(62) 80987654321</span>
                            </div>
                            <div class="inbox__right__conversation__head__search">
                                <label for="searchConversation" class="fa fa-search"></label>
                            </div>
                        </div>
                        <div class="inbox__right__conversation__body">
                            <div class="ballon-conv">
                                <span class="_note center-align">Senin, 07 Agustus 2018 • 14:22</span>
                            </div>
                            <div class="ballon-conv ballon-conv--left">
                                <div class="_content">
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laboriosam numquam quisquam porro! Blanditiis, corporis error. Modi mollitia, culpa laudantium, neque rerum nulla aut distinctio vel tempora non omnis consequatur sapiente?
                                </div> 
                                <span class="_note _alert">Tidak terkirim, 14:22</span>
                            </div>

                            <div class="ballon-conv ballon-conv--right">
                                <div class="_content">
                                    Quisquam porro! Blanditiis, corporis error. Modi mollitia.
                                </div> 
                                <div class="_content">
                                    Culpa laudantium, neque rerum nulla aut distinctio vel tempora non omnis consequatur sapiente?
                                </div> 
                                <span class="_note">14:23</span>
                            </div>

                            <div class="ballon-conv ballon-conv--left">
                                <div class="_content">
                                    Neque rerum nulla aut distinctio vel tempora non omnis consequatur sapiente?
                                </div> 
                                <span class="_note">Terkirim, 14:26</span>
                            </div>
                        </div>
                        <div class="inbox__right__conversation__input flexed">
                            <div class="flexed__1-w">
                                <input type="text" placeholder="Tulis Pesan">
                            </div>
                            <div>
                                <button>
                                    <img src="/assets/img/icon-send.png">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
