@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">Professional SMS</h1>
        </div>
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{route('pro.store')}}" method="POST">
                    {{csrf_field()}}
                        <div class="form-team kontakpenerima">
                            <label for="receiver">Penerima</label>
                            <label class="_right fbold fcolor-primary" id="penerima">0</label>
                            <input type="text" name='receiver[]' id="receiver" placeholder='Tambahkan Nomor Telepon'>
                        </div>
                        <!-- Tab -->
                        <div class="tab-team2">
                            <div class="tab-team2__head">
                                <ul class="nav tab-team2__head__link-wrapper flexed">
                                    <li class="tab-team2__head__link-wrapper__link flexed__1-w">
                                        <a class="active" data-toggle="pill" href="#contact">Kontak</a>
                                    </li>
                                    <li class="tab-team2__head__link-wrapper__link half-width">
                                        <a class="" data-toggle="pill" href="#group">Grup</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-team2__body tab-content reset-p--bottom">
                                <div class="tab-pane active" id="contact">
                                    <div class="form-team form-team--search-input">
                                        <input type="text" id="name" class="caricontacts" placeholder="Cari Kontak">
                                    </div>
                                    <div class="select-receiver">
                                    @foreach ($contact as $row)
                                        <label class="checkbox-wrapper">
                                            <input type="checkbox" class="contacts_id" name="contacts_id[]" value="{{$row->id}}">
                                            <span class="checkmark"></span>
                                            <div class="select-contact">
                                                <span class="block breakh__bottom--x-small">{{$row->name}}</span>
                                                <span class="block _number">{{$row->phone_number}}</span>
                                            </div>
                                        </label>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="group">
                                    <div class="form-team form-team--search-input">
                                        <input type="text" id="name" class="carigroups" placeholder="Cari Grup">
                                    </div>
                                    <div class="select-receiver">
                                    @foreach ($group as $row)
                                        <label class="checkbox-wrapper">
                                            <input type="checkbox" class="groups_id" name="groups_id[]" value="{{$row->id}}">
                                            <span class="checkmark"></span>
                                            <div class="select-contact">
                                                <span class="block breakh__bottom--x-small">{{$row->name}}</span>
                                                <span class="block _number">{{ $row->contact->count() }} Member</span>
                                            </div>
                                        </label>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div><!-- Tab / -->
                </div>
                <div class="col-sm-6">
                        <div class="form-team">
                            <label for="masking">Masking</label>
                            <div class="form-team--arrow">
                                <select id="masking" name="masking_id" class="maskingselect">
                                    <option value="">- Pilihan -</option>
                                   @foreach($masking as $row)
                                   <option value="{{$row->id}}" data-user="{{$row->username}}" data-pass="{{$row->password}}">{{$row->name}}</option>
                                   @endforeach
                                </select>
                                <input type="hidden" name="username" id="smsuser">
                                <input type="hidden" name="password" id="smspass">
                            </div>
                        </div>
                        <div class="form-team">
                            <label for="topupValue">Campaign Name</label>
                            <input type="text" id="topupValue" name="name" placeholder="Masukan Nama Campaign" value="{{$kodecampaign}}" readonly>
                        </div>
                        <div class="form-team">
                            <label for="template">Template Pesan</label>
                            <label class="_right">Optional</label>
                            <div class="form-team--arrow">
                                <select id="template" class="messages-template" name="template_id">
                                    <option value="">- Pilihan -</option>
                                    @foreach ($template as $rowtemplate)
                                    <option value="{{$rowtemplate->id}}" data-isi="{{$rowtemplate->content}}">{{$rowtemplate->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-team">
                            <label for="messages">Pesan</label>
                            <textarea id="messages" placeholder="Tulis deskripsi atau catatan" name="messages"></textarea>
                            <div class="flexed fcolor-disabled">
                                <div class="flexed__1-w">SMS: <span id="cmessages">1</span></div>
                                <div>
                                    <span id="ctotal">0</span>/160
                                </div>
                            </div>
                        </div>
                        <div class="form-team">
                            <div class="flexed">
                                <label for="messages" class="flexed__1-w">Scheduled</label>
                                <div class="switch-checkbox-wrapper">
                                    <label class="switch-checkbox" for="checkbox">
                                        <input type="checkbox" id="checkbox" name="scheduler_status"/>
                                        <div class="slider-switch-checkbox round"></div>
                                    </label>
                                </div>
                            </div>
                            <div class="row row--medium-med time-date-wrapper">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="">
                                        <div class="form-team--date">
                                            <input class="datepicker" type="text" name="scheduler_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="">
                                        <div class="form-team--time">
                                            <input id='timepicker' type='text' name='time_date'/>
                                            <input type='hidden' name='time_kita'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="link-btn link-btn__negative btnPreview" data-toggle="modal" data-target="#popupPreview">
                                    Preview
                                </button>
                                <!-- Popup -->
                                <div class="modal fade" id="popupPreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Preview</div>
                                                    <span class="block">
                                                        Berikut adalah preview pesan yang akan dikirimkan
                                                    </span>
                                                    <div class="preview-message">
                                                    </div>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Kirim">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                            <div class="col-6">
                                <button type="button" class="link-btn link-btn__primary shadow" data-toggle="modal" data-target="#popupSubmit">
                                    Kirim
                                </button>
                                <!-- Popup -->
                                <div class="modal fade" id="popupSubmit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <center>
                                                    <div class="fsize-p-4 fbold breakh__bottom--small">Kirim Pesan?</div>
                                                    <span class="block">
                                                        Selalu periksa kembali pesan yang akan dikirimkan, jika sudah benar klik tombol kirim
                                                    </span>
                                                </center>
                                                <div class="row breakh__top--medium-med">
                                                    <div class="col-6">
                                                        <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="submit" class="link-btn link-btn__primary shadow" value="Kirim">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Popup /-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
