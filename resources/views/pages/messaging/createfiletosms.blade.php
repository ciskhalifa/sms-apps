@extends('layout.default')
@section('content')
@include('include.sidebar')
<div class="col-12 col-lg-8">
    <div class="wrapper wrapper--rounded right-side-wrapper shadow">
        <div class="flexed flexed--sm wrapper--padding--large">
            <h1 class="flexed__1-w fsize-p-18 fbold">File To SMS</h1>
            <div class="breakv d-lg-none"></div>
            <ul class="nav tab-team2__head__link-wrapper">
                <li class="tab-team2__head__link-wrapper__link">
                    <a class="fcolor-primary" href="{{route('sample.download')}}" target="_BLANK">Download Sample</a>
                </li>
                <li class="tab-team2__head__link-wrapper__link">
                <a class="active" href="#" data-toggle="modal" data-target="#popupImport">Upload File</a>
                </li>
            </ul>
        </div>
        <!-- Tab -->
        <div class="wrapper--main-body wrapper--padding--large-equal">
            <form action="{{route('filetosms.store')}}" method="POST">
            {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-team">
                            <label for="topupValue">Campaign Name</label>
                            <input type="text" id="topupValue" placeholder="Masukan Nama Campaign" name="campaign_name" value="{{$kodecampaign}}">
                        </div>
                        <div class="form-team">
                            <label for="fmasking">Masking</label>
                            <div class="form-team--arrow">
                                <select id="masking" name="masking" class="maskingselect">
                                    <option>- Pilihan -</option>
                                    @foreach ($masking as $rowmasking)
                                    <option value="{{$rowmasking->id}}" data-user="{{$rowmasking->username}}" data-pass="{{$rowmasking->password}}">{{$rowmasking->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="username" id="smsuser">
                    <input type="hidden" name="password" id="smspass">
                    <div class="col-12">
                        <table class="table-team dt-responsive tblfiletosms" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Penerima</th>
                                    <th>Pesan</th>
                                    <th>Tanggal Dikirim</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="col-6 offset-6">
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('filetosms')}}" class="link-btn link-btn__negative">Batal</a>
                            </div>
                            <div class="col-6">
                                <input type="submit" class="link-btn link-btn__primary" value="Simpan">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- Tab / -->
    </div>
</div>
<!-- Popup Contact Import -->
<div class="modal fade" id="popupImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form id="xfrmkontak" name="xfrmkontak" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <label for="upload-img-btn" class="link-btn link-btn--small link-btn__tertiary shadow reset-m">Pilih File</label>
                    <input type="file" name="filetosms" id="upload-img-btn" class="hide" placeholder="Tidak ada yang dipilih" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <div class="tabled">
                        <span class="tabled tabled__cell tabled__cell--middle fsize-m-2 fcolor-disabled file-label" style="height:3.6rem">Tidak ada yang dipilih</span>
                    </div>
                    <div class="row breakh__top--medium-med">
                        <div class="col-6">
                            <a href="#!" class="link-btn link-btn__negative" data-dismiss="modal">Batal</a>
                        </div>
                        <div class="col-6">
                            <button type="button" id="ximportkontak" class="link-btn link-btn__secondary shadow">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- Popup Contact Import /-->
@endsection
