@include('include.navbar')
<div class="popup-bg"></div>
<div class="drawer-wrapper d-lg-none">
    <div class="nav-bar-drawer">
        <div class="nav-bar-drawer__notification hide container">
            <div class="flexed quick-view__user">
                <span class="icon--large quick-view__user__text">
                    <i class="btn-close-notif-drawer fa fa-arrow-left"></i>
                </span>
                <span class="fsize-p-4 fbold flexed__1-w quick-view__user__name quick-view__user__text">Notifications</span>
            </div>
        </div>

        <div class="nav-bar-drawer__default container">
            <div class="flexed quick-view__user">
                <img src="{{url('assets/img/user.jpg')}}" alt="Avatar" class="avatar avatar--medium">
                <span class="fsize-p-4 fbold flexed__1-w quick-view__user__name quick-view__user__text">Myoui Mina</span>
                <div class="icon">
                    <span class="icon--notif btn-notif-drawer icon--large quick-view__user__text fcolor-primary">
                        <i class="fa fa-bell"></i>
                    </span>
                </div>
                <div class="icon drawer-btn-close">
                    <span class="icon--large quick-view__user__text breakh__left--small">
                        <i class="fa fa-times"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="body-drawer">
        <div class="body-drawer__notification hide">
            <a href="#!" class="notification__list">
                <div class="flexed">
                    <span class="flexed__1-w _name">Dian Kusuma</span>
                    <span class="_time">21.22</span>
                </div>
                <span class="_des">Lorem ipsum dolor sit amet</span>
            </a>
            <a href="#!" class="notification__list">
                <div class="flexed">
                    <span class="flexed__1-w _name">David</span>
                    <span class="_time">Sabtu, 11.36</span>
                </div>
                <span class="_des">Lorem ipsum dolor sit amet</span>
            </a>
            <a href="#!" class="notification__list notification__list--unread">
                <div class="flexed">
                    <span class="flexed__1-w _name">Sana Minari</span>
                    <span class="_time">11/12/2018</span>
                </div>
                <span class="_des">Lorem ipsum dolor sit amet</span>
            </a>
            <a href="#!" class="notification__list">
                <div class="flexed">
                    <span class="flexed__1-w _name">Michelle Agustin</span>
                    <span class="_time">08/12/2018</span>
                </div>
                <span class="_des">Lorem ipsum dolor sit amet</span>
            </a>
        </div>

        <div class="body-drawer__default container">
            <div class="row">
                <div class="col-12">
                    <div class="quick-view__progressbar">
                        <div class="progressbar-team">
                            <div class="progressbar-team__top fcolor-primary fbold">SMS Balance</div>
                            <div class="progress progress--primary">
                                <div class="progress-bar" style="width:67%"></div>
                            </div>
                            <div class="progressbar-team__bottom flexed">
                                <span class="flexed__1-w">482 SMS</span>
                                <span class="fcolor-disabled">23 Jan 2019</span>
                            </div>
                        </div>              
                        <div class="progressbar-team">
                            <div class="progressbar-team__top fcolor-secondary fbold">Whatsapp Balance</div>
                            <div class="progress progress--secondary">
                                <div class="progress-bar" style="width:32%"></div>
                            </div>
                            <div class="progressbar-team__bottom flexed">
                                <span class="flexed__1-w">234 Chat</span>
                                <span class="fcolor-disabled">23 Jan 2019</span>
                            </div> 
                        </div>             
                    </div>
                    <div id="side-menu-drawer" class="side-menu drawer-user-menu">
                        <a class="side-menu__head-link fsize-p-4 fbold block collapsed" data-toggle="collapse" href="#account">
                            Account
                        </a>
                        <div id="account" class="collapse side-menu__body" data-parent="#side-menu-drawer">
                            <a href="{{url('account')}}" class="side-menu__body__link"><i class="breakh__right fa fa-user"></i>Account Setting</a>
                            <a href="{{url('topup')}}" class="side-menu__body__link"><i class="breakh__right fa fa-refresh"></i>Top Up Balance</a>
                            <a href="{{url('masking')}}" class="side-menu__body__link"><i class="breakh__right fa fa-paper-plane"></i>Masking Management</a>
                        </div>

                        <a class="side-menu__head-link fsize-p-4 fbold block" data-toggle="collapse" href="#messaging">
                            Messaging
                        </a>
                        <div id="messaging" class="collapse show" data-parent="#side-menu-drawer">
                            <a href="{{url('inbox')}}" class="side-menu__body__link"><i class="breakh__right fa fa-envelope"></i>Inbox</a>
                            <a href="{{url('quicksms')}}" class="side-menu__body__link"><i class="breakh__right fa fa-paper-plane"></i>Quick SMS</a>
                            <a href="{{url('prosms')}}" class="side-menu__body__link"><i class="breakh__right fa fa-envelope"></i>Professional SMS</a>
                            <a href="{{url('filetosms')}}" class="side-menu__body__link"><i class="breakh__right fa fa-file"></i>File To SMS</a>     
                        </div>

                        <a class="side-menu__head-link fsize-p-4 fbold block collapsed" data-toggle="collapse" href="#reports">
                            Reports
                        </a>
                        <div id="reports" class="collapse" data-parent="#side-menu-drawer">
                            <a href="{{url('report/overview')}}" class="side-menu__body__link"><i class="breakh__right fa fa-line-chart"></i>Overview</a>
                            <a href="{{url('report/delivery_status')}}" class="side-menu__body__link"><i class="breakh__right fa fa-check-square"></i>Delivery Status</a>
                        </div>

                        <!--<a class="side-menu__head-link fsize-p-4 fbold block collapsed" data-toggle="collapse" href="#whatsappBlast">-->
                            <!--Whatsapp Blast-->
                        <!--</a>-->
                        <!--<div id="whatsappBlast" class="collapse" data-parent="#side-menu-drawer">-->
                            <!--<a href="{{url('inboxwa')}}" class="side-menu__body__link"><i class="breakh__right fa fa-envelope"></i>Inbox Whatsapp</a>-->
                            <!--<a href="{{url('blastwa')}}" class="side-menu__body__link"><i class="breakh__right fa fa-paper-plane"></i>Whatsapp Blast</a>-->
                            <!--<a href="{{url('filetowa')}}" class="side-menu__body__link"><i class="breakh__right fa fa-file"></i>File To Whatsapp</a>-->
                            <!--<a href="{{url('deliverywa')}}" class="side-menu__body__link"><i class="breakh__right fa fa-check-square"></i>Whatsapp Delivery Status</a>-->
                        <!--</div>-->

                        <a class="side-menu__head-link fsize-p-4 fbold block collapsed" data-toggle="collapse" href="#utilities">
                            Utilities
                        </a>
                        <div id="utilities" class="collapse" data-parent="#side-menu-drawer">
                            <a href="{{url('address')}}" class="side-menu__body__link"><i class="breakh__right fa fa-book"></i>Address Book</a>
                            <a href="{{url('template')}}" class="side-menu__body__link"><i class="breakh__right fa fa-pencil-square"></i>Template Pesan</a>
                        </div>
                    </div>

                    <div class="drawer-user-menu">
                        <a class="side-menu__head-link _no-arrow fsize-p-4 fbold block" href="{{url('account')}}">
                            Profile
                        </a>
                        <a class="side-menu__head-link _no-arrow fsize-p-4 fbold block fcolor-tertiary" href="">
                            Log Out
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>