<footer>
    <div class="container">
        <div class="row">
            <div class="right-align col-12 col-lg-4 offset-lg-4">
                Develop with Love by <a href="http://aksikode.co" target="_blank">aksikode</span>
            </div>
        </div>
    </div>
</footer>
<!-- Javascript -->
<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/inline-svg.js')}}"></script>
<script src="{{url('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/js/dataTables.bootstrap4.setup.js')}}"></script>
<script src="{{url('assets/js/dataTables.select.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/js/Chart.min.js')}}"></script>
<script src="{{url('assets/js/timepicki.js')}}"></script>
<script src="{{url('assets/js/jquery.validate.min.js')}}"></script>
<script src="{{url('assets/js/jquery.validate-custom.js')}}"></script>
<script src="{{url('assets/js/messages-count.js')}}"></script>
<script src="{{url('assets/js/tagify.js')}}"></script>
<script src="{{url('assets/js/tagify.custom.js')}}"></script>
<script src="{{url('assets/js/add-input.js')}}"></script>
<script src="{{url('assets/js/upload-img.js')}}"></script>
<script src="{{url('assets/js/main.js')}}"></script>
<script src="{{url('assets/js/bootstrap-growl.min.js')}}"></script>
<script src="{{url('assets/js/notify.js')}}"></script>
<script src="{{url('assets/js/jquery.loading.min.js')}}"></script>
<script src="{{url('assets/js/timepicki.active.js')}}"></script>
<script src="{{url('assets/js/custom.js')}}"></script>

<!-- Javascript /-->
<script>
    $(document).ready(function(){
        var i = 0;
        var progsms = $('.progressbarsms');
        var id;
        @if(Auth::check())
            id = '{{auth()->user()->id}}';
        @endif
        var url =  window.location.origin + "/checkcredits";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'GET',
            url: url,
            data: {'username': id},
            dataType: 'json',
            success: function (data) {
                $('.creditsms').html(data.value + " SMS");
                $('.tglcredit').html(data.expiredDate);
                function countNumbers(){
                    if(i < 100){
                        i = data.value;
                        progsms.css("width", i + "%");
                        progsms.attr('aria-valuenow', data.value);
                    }
                    // Wait for sometime before running this script again
                   
                }
                countNumbers();
            }
        });
    });
</script>
</body>
</html>
