<nav id="nav-bar">
    <div class="container flexed">
        <div id="nav-bar__left">
            <img src="{{url('assets/img/logo.svg')}}" alt="Logo" id="nav-bar__left__logo">
        </div>
        <div id="nav-bar__right" class="tabled flexed__1-w">
            <div class="tabled__cell tabled__cell--middle">
                <div class="flexed right">
                    <a href="topup-balance-input.html" class="link-btn link-btn__secondary link-btn--medium link-btn--padding shadow breakh__right--medium d-none d-lg-block"><i class="fa fa-download"></i> Top Up Balance</a>

                    <div class="link-btn-expand-wrapper d-none d-lg-block">
                        <div class="link-btn link-btn__quaternary link-btn--medium link-btn--padding link-btn--expand link-btn--expand--arrow shadow">
                            <img src="{{url('assets/img/user.jpg')}}" alt="Avatar" class="avatar avatar--small breakh__right--small"><span class="breakh__right--big">Myoui Mina</span>
                        </div>

                        <div class="link-btn-expand-wrapper__content shadow">
                            <div class="link-btn-expand-wrapper__content__block-btn link-btn-expand-wrapper__content__block-btn--medium"></div>
                            <div class="link-btn-expand-wrapper__content__main">
                                <span class="link-btn-expand-wrapper__content__main__link"><a href="{{url('account')}}">Profile</a></span>
                                <span class="link-btn-expand-wrapper__content__main__link"><a href="{{url('inbox')}}">Messaging</a></span>
                                <span class="link-btn-expand-wrapper__content__main__link"><a class="link-btn-expand-wrapper__content__main__link--unique" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Log Out</a></span>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="icon drawer-btn d-lg-none">
                        <span class="icon--notif icon--large quick-view__user__text">
                            <i class="fa fa-bars"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</nav>