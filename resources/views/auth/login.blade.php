@extends('layout.defaultlogin')
@section('content')
<div class="row">
    <div class="col-12 col-lg-4 offset-lg-4">
        <div class="wrapper wrapper--rounded wrapper--padding wrapper--offset shadow">
            <div class="login-logo-wrapper">
                <img src="/assets/img/logo.svg" alt="Logo">
            </div>
            <form class="vali" action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-team">
                    <label for="fusername">Username</label>
                    <input type="text" id="fusername" name="username" placeholder="Masukan Username" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-team">
                    <label for="fpassword">Kata Sandi</label>
                    <input type="password" id="fpassword" name="password" placeholder="Masukan Kata Sandi" required>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="wrapper--offset__btn-wrapper--large">
                    <input type="submit" value="Masuk" class="link-btn link-btn__primary shadow">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection