
//Custom JS
$('.txtarea').keypress(function (event) {
    if (event.which == 13) {
        event.stopPropagation();
    }
});

$('.btnPreview').on('click', function () {
    $('.preview-message').html('<p align="justify">' + $('#messages').val() + '</p>');

});

$(".master").bind('click', function () {
    var judul = $('.judul').val();
    if ($(this).is(':checked', true)) {
        var a = $(".sub_chk").prop('checked', true);
        $('.lblpilih').html(a.length + " " + judul + " dipilih");
    } else {
        var a = $(".sub_chk").prop('checked', false);
        $('.lblpilih').html("0 " + judul + " dipilih");
    }
});

$(".sub_chk").bind('click', function () {
    var x = [];
    var judul = $('.judul').val();
    if ($('.sub_chk:checked').length == $('.sub_chk').length) {
        $('.master').prop('checked', true);
        x.push($('.sub_chk:checked').val());
    } else {
        $('.master').prop('checked', false);
    }
    $(".lblpilih").html($("input.sub_chk:checked").length + " " + judul + " dipilih");

});

$('.btnDelAll').on('click', function () {
    var data = [];
    $(".sub_chk:checked").each(function () {
        data.push($(this).data('id'));
    });
    if (data.length <= 0) {
        alert('Please select record');
    } else {
        $('#cid').val(data);
    }
});

$('.btnDel').bind('click', function () {
    var nilai = $(this).attr('data-default');
    $('#cid').val(nilai);
});




// JS ADDRESSBOOK
$(".masterkontak").bind('click', function () {
    var judul = $('.judul').val();
    if ($(this).is(':checked', true)) {
        var a = $(".sub_chk_kontak").prop('checked', true);
        $('.lblpilih').html(a.length + " " + judul + " dipilih");
    } else {
        var a = $(".sub_chk_kontak").prop('checked', false);
        $('.lblpilih').html("0 " + judul + " dipilih");
    }
});

$(".sub_chk_kontak").bind('click', function () {
    var x = [];
    var judul = $('.judul').val();
    if ($('.sub_chk_kontak:checked').length == $('.sub_chk_kontak').length) {
        $('.masterkontak').prop('checked', true);
        x.push($('.sub_chk_kontak:checked').val());
    } else {
        $('.masterkontak').prop('checked', false);
    }
    $(".lblpilih").html($("input.sub_chk_kontak:checked").length + " " + judul + " dipilih");

});

// TAMBAH GROUP HALAMAN KONTAK
$(".ceklisgroup").bind('click', function () {
    var z = [];
    var g = [];
    $(".sub_chk_kontak:checked").each(function () {
        z.push($(this).val());
    });
    $('.ceklisgroup:checked').each(function () {
        g.push($(this).val());
        // $('#loading').removeClass('hide');
    });
    if (z.length <= 0) {
        alert('Please select record');
        $(this).prop("checked", false);
    } else {

    }
    if ($(this).is(':checked') == true) {
        var i = $(this).val();
        showLoader(i);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: window.location.origin + "/address/grouping",
            data: { "group": g, "kontak": z },
            type: "POST",
            dataType: 'html',
            cache: "false",
            beforesend: function () { showLoader(i) },
            complete: function () { hideLoader(i) },
            success: function () { }
        })
    } else {
        hideLoader(i);
    }

});
//

$(".mastergroup").bind('click', function () {
    var judul = $('.judul').val();
    if ($(this).is(':checked', true)) {
        var a = $(".sub_chk_group").prop('checked', true);
        $('.lblpilih').html(a.length + " " + judul + " dipilih");
    } else {
        var a = $(".sub_chk_group").prop('checked', false);
        $('.lblpilih').html("0 " + judul + " dipilih");
    }
});

$(".sub_chk_group").bind('click', function () {
    var x = [];
    var judul = $('.judul').val();
    if ($('.sub_chk_group:checked').length == $('.sub_chk_group').length) {
        $('.mastergroup').prop('checked', true);
        x.push($('.sub_chk_group:checked').val());
    } else {
        $('.mastergroup').prop('checked', false);
    }
    $(".lblpilih").html($("input.sub_chk_group:checked").length + " " + judul + " dipilih");

});

$('.btnDelAllGrp').on('click', function () {
    var data = [];
    $(".sub_chk_group:checked").each(function () {
        data.push($(this).data('id'));
    });
    if (data.length <= 0) {
        alert('Please select record');
        $('#popupDeleteGroup').hide();
    } else {
        $('#cidgrp').val(data);
    }
});

$('.btnDelGrp').bind('click', function () {
    var nilai = $(this).attr('data-default');
    $('#cidgrp').val(nilai);
});

// IMPORT FORM GROUP
function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="selectall"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

var rows_selected = [];
var xdata = [];
var tablekontak = $('.tblkontak').DataTable({
    "dom":
        '<"dt-custom _offset"<"dt-custom__filter"f><"dt-custom__table"t><"dt-custom__paging"p><"dt-custom__info"i><"dt-custom__length"l>>',
    "language": {
        "paginate": {
            "previous": "<",
            "next": ">"
        },
        "lengthMenu": "Display _MENU_ records per page",
        "zeroRecords": "Data tidak ditemukan.",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
    },
    ordering: false,
    processing: true,
    serverSide: true,
    ajax: window.location.origin + "/address/json",
    columns: [
        { 'data': 'id' },
        { 'data': 'name' },
        { 'data': 'phone_number' }
    ],
    columnDefs: [{
        orderable: false,
        targets: 0,
        render: function (data, type, full, meta) {
            return '<label class="checkbox-wrapper"> <input name="contacts_id[]" data-name= "' + full.name + '" data-id="' + data + '" value="' + full.phone_number + '" type="checkbox" class="sub_chk_contact"> <span class="checkmark"></span></label>';
        }
    }],
    rowCallback: function (row, data, dataIndex) {
        // Get row ID
        var rowId = data[0];
        // If row ID is in the list of selected row IDs
        if ($.inArray(rowId, rows_selected) !== -1) {
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
        }
    }
});
// Handle click on checkbox
$('.tblkontak tbody').on('click', 'input[type="checkbox"]', function (e) {
    var judul = $('.judul').val();
    var $row = $(this).closest('tr');
    var xcontact_name = [];
    var xcontact_number = [];
    // Get row data
    var data = tablekontak.row($row).data();
    // Get row ID
    var rowId = data[0];
    // Determine whether row ID is in the list of selected row IDs
    var index = $.inArray(rowId, rows_selected);
    // If checkbox is checked and row ID is not in list of selected row IDs
    if (this.checked && index === -1) {
        rows_selected.push(rowId);
        // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
    } else if (!this.checked && index !== -1) {
        rows_selected.splice(index, 1);
    }
    if (this.checked) {
        $row.addClass('selected');
        $(".lblpilih").html($('input[type="checkbox"]:checked').length + " " + judul + " dipilih");
        $('input[type="checkbox"]:checked').each(function () {
            xcontact_number.push($(this).val());
            xcontact_name.push($(this).data('name'));
        });
        $(".contacts_number").val(xcontact_number);
        $(".contacts_name").val(xcontact_name);

    } else {
        $row.removeClass('selected');
        $(".lblpilih").html($('input[type="checkbox"]:checked').length + " " + judul + " dipilih");
        $('input[type="checkbox"]:checked').each(function () {
            xcontact_number.push($(this).val());
            xcontact_name.push($(this).data('name'));
        });
        $(".contacts_number").val(xcontact_number);
        $(".contacts_name").val(xcontact_name);
    }
    // Update state of "Select all" control
    updateDataTableSelectAllCtrl(tablekontak);
    // Prevent click event from propagating to parent
    e.stopPropagation();
});

// Handle click on table cells with checkboxes
$('.tblkontak').on('click', 'tbody td, thead th:first-child', function (e) {
    $(this).parent().find('input[type="checkbox"]').trigger('click');
});

// Handle click on "Select all" control
$('thead input[name="selectall"]', tablekontak.table().container()).on('click', function (e) {
    if (this.checked) {
        $('.tblkontak tbody input[type="checkbox"]:not(:checked)').trigger('click');
    } else {
        $('.tblkontak tbody input[type="checkbox"]:checked').trigger('click');
    }
    // Prevent click event from propagating to parent
    e.stopPropagation();
});

// Handle table draw event
tablekontak.on('draw', function () {
    // Update state of "Select all" control
    updateDataTableSelectAllCtrl(tablekontak);
});

$('#ximport').on('click', function () {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window.location.origin + "/address/readFile",
        data: new FormData($('#xfrm')[0]),
        type: "POST",
        processData: false,
        contentType: false,
        dataType: 'html',
        success: function (html) {
            tablekontak.ajax.reload();
            $('#popupImport').modal('hide');
        }
    })
});

var tablefiletosms = $('.tblfiletosms').DataTable({
    "dom":
        '<"dt-custom"<"dt-custom__filter"f><"dt-custom__table"t><"dt-custom__paging"p><"dt-custom__info"i><"dt-custom__length"l>>',
    "language": {
        "paginate": {
            "previous": "<",
            "next": ">"
        },
        "lengthMenu": "Display _MENU_ records per page",
        "zeroRecords": "Data tidak ditemukan.",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
    },
    ordering: false,
    filter: false,
    processing: true,
    serverSide: true,
    ajax: window.location.origin + "/filetosms/json",
    columns: [
        { 'data': 'phone_number' },
        { 'data': 'pesan' },
        { 'data': 'tgl_kirim' }
    ]
});

$('#ximportkontak').on('click', function () {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window.location.origin + "/filetosms/readFile",
        data: new FormData($('#xfrmkontak')[0]),
        type: "POST",
        processData: false,
        contentType: false,
        dataType: 'html',
        success: function (html) {
            tablefiletosms.ajax.reload();
            $('#popupImport').modal('hide');
        }
    })
});
//
// QUICK SMS 
$('.maskingselect').on('change', function () {
    $('#smsuser').val($("option:selected", this).data("user"));
    $('#smspass').val($("option:selected", this).data("pass"));
});

// PRO SMS
$('.contacts_id').change(function () {
    if (this.checked) {
        console.log(this);
        $('.kontakpenerima').addClass('hide');
    } else {
        $('.kontakpenerima').removeClass('hide');
    }
});
$('.groups_id').change(function () {
    if (this.checked) {
        $('.kontakpenerima').addClass('hide');
    } else {
        $('.kontakpenerima').removeClass('hide');
    }
});

// File To SMS
$('.btnDelFiletoSmsindex').bind('click', function () {
    var nilai = $(this).attr('data-default');
    $('#cidfiletosms').val(nilai);
});
