// // Upload Img
// function readURL(input) {
// 	if (input.files && input.files[0]) {
//     var reader = new FileReader();

//     reader.onload = function (e) {
//     	$('#uploadImg').attr('src', e.target.result);
//     }
//     reader.readAsDataURL(input.files[0]);
// 	}
// }

// $("#upload-img-btn").change(function(){
// 	readURL(this);
// });

// //Remove Img
// function removeImg()
// {
// var img = document.getElementById("uploadImg");
// img.src="img/logo.png";
// return false;
// }


$('#image').change(function () {
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
        readURL(this);
    else
        alert("Please select image file (jpg, jpeg, png).")
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
            //$('#remove').val(0);
        }
    }
}