//Tagify
function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
var datanumber = [];
var pattern = new RegExp("^[0-9]*$");
var formReceiver = document.querySelector('input[id=receiver]'),
    tagReceiver = new Tagify(formReceiver, {
        enforeWhitelist: true,
        whitelist: ["Riyadh", "Ricky", "Riyadi", "081234567890", "Keluarga", "Jihyo", "Sana"],
        pattern: pattern
    }).on('add', function (e, value) {
        //console.log('added', e.detail.value);
        $('.tab-team2').css("pointer-events: none;opacity: 0.4;");
        datanumber.push(e.detail.value);
        $('#receiver').val(datanumber);
        $('#penerima').html(datanumber.length);
        $('.contacts_id').attr('disabled', 'disabled');
        $('.groups_id').attr('disabled', 'disabled');
        $('.carigroups').attr('disabled', 'disabled');
        $('.caricontacts').attr('disabled', 'disabled');
        $('.caricontacts').val('Disabled');
    }).on('remove', function (e, value) {
        //console.log('remmoved', e.detail.value);
        $('.tab-team2').removeAttr("style");
        removeA(datanumber, e.detail.value);
        $('#receiver').val(datanumber);
        $('#penerima').html(datanumber.length);
        if (datanumber.length == '0'){
            $('.contacts_id').removeAttr('disabled', 'disabled');
            $('.groups_id').removeAttr('disabled', 'disabled');
            $('.carigroups').removeAttr('disabled', 'disabled');
            $('.caricontacts').removeAttr('disabled', 'disabled');
            $('.caricontacts').val('');
           
        }
    });
