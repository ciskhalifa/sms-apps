$(document).ready(function(){

    part1Count = 160;
    part2Count = 160;
    part3Count = 160;

    $('#messages').keyup(function(){
        var chars = $(this).val().length;
            cmessages = 0;
            remaining = 0;
            ctotal = 0;
        if (chars <= part1Count) {
            cmessages = 1;
            remaining = part1Count - chars;
        } else if (chars <= (part1Count + part2Count)) {
            cmessages = 2;
            remaining = part1Count + part2Count - chars;
        } else if (chars > (part1Count + part2Count)) {
            moreM = Math.ceil((chars - part1Count - part2Count) / part3Count) ;
            remaining = part1Count + part2Count + (moreM * part3Count) - chars;
            cmessages = 2 + moreM;
        }
        $('#remaining').text(remaining);
        $('#cmessages').text(cmessages);
        $('#ctotal').text(chars);
        if (remaining > 1) $('.cplural').show();
            else $('.cplural').hide();
        if (cmessages > 1) $('.mplural').show();
            else $('.mplural').hide();
        if (chars > 1) $('.tplural').show();
            else $('.tplural').hide();
    });
    $('#messages').keyup();
});
