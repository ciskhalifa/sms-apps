function notify(a, t) {
	$.growl({
		message: a
	}, {
			type: t,
			allow_dismiss: !1,
			label: "Cancel",
			className: "btn-xs btn-inverse",
			placement: {
				from: "top",
				align: "right"
			},
			delay: 2500,
			animate: {
				enter: "animated bounceIn",
				exit: "animated bounceOut"
			},
			offset: {
				x: 20,
				y: 85
			}
		})
}