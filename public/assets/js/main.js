//Dropdown User Menu
$('.link-btn-expand-wrapper').click(function () {
    $(this).find('.link-btn-expand-wrapper__content').addClass('_expanded');
    $(this).find('.link-btn--expand--arrow').addClass('_expanded');
});

$('html').click(function () {
    $('.link-btn-expand-wrapper__content').removeClass('_expanded');
    $('.link-btn--expand--arrow').removeClass('_expanded');
});

$('.link-btn-expand-wrapper').click(function (event) {
    event.stopPropagation();
});

//Upload File
$("[type=file]").on("change", function () {
    // Name of file and placeholder
    var file = this.files[0].name;
    var dflt = $(this).attr("placeholder");
    if ($(this).val() != "") {
        $(".file-label").text(file);
    } else {
        $(".file-label").text(dflt);
    }
});

//Notifications
$(".icon--notif, .notification").on({
    mouseenter: function () {
        $('.notification').addClass('_active');
    },
    mouseleave: function () {
        $('.notification').removeClass('_active');
    }
});

//Radio Wrapper
$('.radio-wrapper').click(function () {
    $(this).parents(".form-team").find('.radio-wrapper__radio').removeClass('_active');
    $(this).find('.radio-wrapper__radio').addClass('_active');
});

//Loader 
function showLoader(urutan) {
    $('.loading' + urutan).removeClass('hide');
}

function hideLoader(urutan) {
    $('.loading' + urutan).addClass('hide');
}

//Navbar
$(function () {
    var header = $("#nav-bar");
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 1) {
            header.addClass("nav-bar-scrolled").addClass("shadow");
        } else {
            header.removeClass("nav-bar-scrolled").removeClass("shadow");
        }
    });
});

//Drawer Menu
$('.drawer-btn').click(function () {
    $('.popup-bg').addClass("_active");
    $('.drawer-wrapper').addClass("_active");
    $("body").css("overflow", "hidden");
});

$('.drawer-btn-close, .popup-bg').click(function () {
    $('.popup-bg').removeClass("_active");
    $('.drawer-wrapper').removeClass("_active");
    $("body").css("overflow", "auto");
});

//Sidebar Drawer
// $(function(){
//     var head = $('.side-menu__body');
//     $('.side-menu__body__link').on('click', function(){
//         head.addClass('show');
//         $('.side-menu__body__link').removeClass("active");
//         $(this).addClass('_active');
//     });
// });
//Navbar Drawer
$(function () {
    var header = $(".nav-bar-drawer");
    $(".body-drawer").scroll(function () {
        var scroll = $(".body-drawer").scrollTop();
        if (scroll >= 1) {
            header.addClass("nav-bar-drawer-scrolled").addClass("shadow");
        } else {
            header.removeClass("nav-bar-drawer-scrolled").removeClass("shadow");
        }
    });
});

//Drawer Notification
$('.btn-notif-drawer').click(function () {
    $('.nav-bar-drawer__notification, .body-drawer__notification').removeClass("hide");
    $('.nav-bar-drawer__default, .body-drawer__default').addClass("hide");
});
$('.btn-close-notif-drawer').click(function () {
    $('.nav-bar-drawer__notification, .body-drawer__notification').addClass("hide");
    $('.nav-bar-drawer__default, .body-drawer__default').removeClass("hide");
});

// Messages Template
$(".messages-template").change(function () {
    var text = $('#messages');
    text.val('');
    text.val(text.val() + $("option:selected", this).data("isi"));
});

//Scedule
$('.switch-checkbox input').on('change', function () {
    if ($('.switch-checkbox input').is(':checked')) {
        $('.time-date-wrapper').addClass('_show');
    } else {
        $('.time-date-wrapper').removeClass('_show');
    }
});

//Address Book
$('.btn-tab-c').click(function () {
    $('.add-c').addClass("_active");
    $('.add-g').removeClass("_active");
    $('.export').attr('href', window.location.origin + "/address/exportkontak");
    $('form[name="import"]').attr('action', window.location.origin + "/address/importkontak");
});
$('.btn-tab-g').click(function () {
    $('.add-g').addClass("_active");
    $('.add-c').removeClass("_active");
    $('.export').attr('href', window.location.origin + "/address/exportgroup");
    $('form[name="import"]').attr('action', window.location.origin + "/address/importgroup");
});