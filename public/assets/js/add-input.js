$(document).ready(function () {
    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).prepend('<div class="add-input"><div class="form-team--arrow breakh__bottom"> <select name="groups_id[]" id="selectgroup' + x + '">' + selectGroup(x) + '</select></div> <i class="fa fa-trash remove_field"></i></div>'); //add input box
        }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })

    $(".remove_field").click(function () {
        $(this).parents('.add-input').remove();
    });
});

function selectGroup(urutan) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window.location.origin + "/address/fill_select_group",
        type: "POST",
        success: function (html) {
            json = eval(html);
            $("#selectgroup" + urutan + " ").append('<option value="">- Pilihan -</option>');
            $(json).each(function () {
                $("#selectgroup" + urutan + " ").append('<option value="' + this.id + '">' + this.disp + "</option>");
                $("#selectgroup" + urutan + " ").append('<i class="fa fa-trash remove_field"></i>');
            });
        }
    })
}

