//Datepicker
$(function () {
    $(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker('update', new Date());
});

//Datatables
$(document).ready(function () {
    $('.dtr').DataTable(
        {
            "dom":
                '<"dt-custom _offset"<"dt-custom__filter"f><"dt-custom__table"t><"dt-custom__paging"p><"dt-custom__info"i><"dt-custom__length"l>>',
            "language": {
                "paginate": {
                    "previous": "<",
                    "next": ">"
                },
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Data tidak ditemukan.",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            },
            "ordering": false
        }
    );

    $('.dtr-no-filter').DataTable(
        {
            "dom":
                '<"dt-custom"<"dt-custom__filter"f><"dt-custom__table"t><"dt-custom__paging"p><"dt-custom__info"i><"dt-custom__length"l>>',
            "language": {
                "paginate": {
                    "previous": "<",
                    "next": ">"
                },
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Data tidak ditemukan.",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            },
            "ordering": false,
            "filter": false
        }
    );
});