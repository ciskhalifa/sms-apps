$().ready(function() {
    // 
    $(".vali").validate({
        rules: {
            fullname: "required",
            username: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 5
            },
            new_password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#new_password"
            },
            email: {
                required: true,
                email: true
            },
            phonenumber: {
                required: true,
                number: true
            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            fullname: "Nama lengkap wajib diisi",
            username: {
                required: "Silahkan masukan username",
                minlength: "Username Anda harus terdiri dari setidaknya 3 karakter"
            },
            password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter"
            },
            new_password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter"
            },
            confirm_password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter",
                equalTo: "Tidak sama dengan kata sandi baru"
            },
            email: {
                required: "Email wajib diisi",
                email: "Masukan email dengan benar"
            },
            phonenumber: {
                required: "Nomor telepon wajib diisi",
                number: "Masukan nomor telepon dengan benar"
            },
            agree: "Please accept our policy",
            topic: "Please select at least 2 topics"
        }
    });
    
    // 
    $("#newPass").validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            new_password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#new_password"
            }
        },
        messages: {
            password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter"
            },
            new_password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter"
            },
            confirm_password: {
                required: "Silahkan masukan kata sandi",
                minlength: "Kata sandi Anda setidaknya harus sepanjang 5 karakter",
                equalTo: "Tidak sama dengan kata sandi baru"
            }
        }
    });
});