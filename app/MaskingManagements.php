<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaskingManagements extends Model
{

    protected $table = 'masking_managements';
    protected $fillable = [
        'id',
        'user_id',
        'type_id',
        'name',
        'description',
        'file',
        'status',
    ];

    public function type()
    {
        return $this->belongsTo(TypeMaskings::class, 'type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function filesms()
    {
        return $this->hasMany(FileToSMSDetail::class, 'maskings_id');
    }
}
