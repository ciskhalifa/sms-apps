<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messagings extends Model
{
    protected $table = 'sms_campaigns';
    protected $fillable = [
        'id',
        'user_id',
        'campaign_type',
        'name',
        'scheduler_status',
        'scheduler_date',
        'status',
    ];
}
