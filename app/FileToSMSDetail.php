<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileToSMSDetail extends Model
{
    protected $table = 'filetosms_details';
    protected $fillable = [
        'id',
        'filetosms_id',
        'user_id',
        'contacts_id',
        'maskings_id',
        'message',
        'phone_number',
        'description',
        'scheduler_date',
        'is_sent'
    ];

    public function masking()
    {
        return $this->belongsTo(MaskingManagements::class, 'maskings_id');
    }

    public function contact()
    {
        return $this->BelongsTo(Contacts::class, 'contacts_id');
    }
    
    public function campaign(){
        return $this->BelongsTo(FileToSMS::class, 'filetosms_id');
    }

}
