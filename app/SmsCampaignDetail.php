<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCampaignDetail extends Model
{
    protected $table = 'sms_campaign_details';
    protected $fillable = [
        'id',
        'user_id',
        'campaign_id',
        'groups_id',
        'contacts_id',
        'templates_id',
        'maskings_id',
        'message',
        'phone_number',
        'description',
        'is_sent',
        'status'
    ];
}
