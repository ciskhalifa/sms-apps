<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileToSMS extends Model
{
    protected $table = 'filetosms';
    protected $fillable = [
        'id',
        'user_id',
        'campaign_name',
        'status',
    ];

    public function campaign()
    {
        return $this->hasMany(FileToSMSDetail::class, 'filetosms_id');
    }

}
