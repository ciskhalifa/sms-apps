<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\GroupContacts;
use App\Groups;
use App\Temp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class AddressBooksController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {

        $data = array(
            'group' => Groups::where('user_id', '=', Auth::id())->get(),
            'contact' => Contacts::where('user_id', '=', Auth::id())->get(),
        );

        return view('pages.address_book.index')->with($data);
    }

    public function createkontak()
    {
        $data = Groups::where('user_id', '=', Auth::id())->get();
        return view('pages.address_book.createkontak', ['group' => $data]);
    }

    public function detail($id)
    {
        $data = array('contact' => Contacts::find($id));
        return view('pages.address_book.detail')->with($data);
    }

    public function creategroup()
    {
        $data = Contacts::where('user_id', '=', Auth::id())->get();
        return view('pages.address_book.creategroup');
    }

    public function editkontak($id)
    {
        $data = array(
            'contact' => Contacts::find($id),
        );
        return view('pages.address_book.editkontak')->with($data);
    }

    public function editgroup($id)
    {
        $data = array(
            'group' => Groups::find($id),
        );
        return view('pages.address_book.editgroup')->with($data);
    }

    public function grouping(Request $request)
    {
        $kontak = $request->input('kontak');
        $group = $request->input('group')[0];
        for ($i = 0; $i < count($kontak); $i++) {
            $users = DB::table('contacts_groups')->select('groups_id', 'contacts_id')->where('groups_id', $group)->where('contacts_id', $kontak[$i])->count();
            if ($users == 0) {
                $datagrp = GroupContacts::create([
                    'user_id' => Auth::id(),
                    'groups_id' => $group,
                    'contacts_id' => $kontak[$i],
                ]);
                $datagrp->save();
                //return redirect('/address')->with('success', 'Data Berhasil di Simpan');
            } else {

            }
        }
    }

    public function simpangroup(Request $request)
    {
        $name = explode(",", $request->input('contacts_name')[0]);
        $number = explode(",", $request->input('contacts_number')[0]);
        $request->validate([
            'name' => 'required',
        ]);
        $data = Groups::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
        $data->save();
        if (is_array($name) && $name[0] != "") {
            for ($i = 0; $i < count($name); $i++) {
                $contacts = DB::table('contacts')->select('user_id', 'phone_number')->where('user_id', Auth::id())->where('phone_number', $number[$i])->count();
               
                if ($contacts == 0) {
                    try {
                        $datakontak = Contacts::create([
                            'user_id' => Auth::id(),
                            'name' => $name[$i],
                            'phone_number' => $number[$i],
                        ]);
                        $datakontak->save();

                        $datagrp = GroupContacts::create([
                            'user_id' => Auth::id(),
                            'groups_id' => $data->id,
                            'contacts_id' => $datakontak->id,
                        ]);
                        $datagrp->save();
                    } catch (\Illuminate\Database\QueryException $e) {
                        $errorCode = $e->errorInfo[1];
                    }
                } else {

                }
                Temp::whereIn('phone_number', $number)->delete();
            }
        } else {
            try {
                $datagrp = GroupContacts::create([
                    'user_id' => Auth::id(),
                    'groups_id' => $data->id,
                    'contacts_id' => '',
                ]);
                $datagrp->save();
            } catch (\Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
            }
            Temp::where('phone_number', $number[0])->delete();
        }
        Alert::success('Data Berhasil Disimpan');
        return redirect('/address');
    }

    public function updategroup(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $data = Groups::find($id);
        $data->name = $request->get('name');
        $data->description = $request->get('description');
        $data->save();
        Alert::success('Data Berhasil Di Update');
        return redirect('/address');
    }

    public function simpankontak(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required',
        ]);
        $data = Contacts::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'phone_number' => $request->input('phone_number'),
        ]);
        $data->save();
        $groups_id = $request->input('groups_id');
        if (isset($groups_id)) {
            if (is_array($groups_id)) {
                for ($i = 0; $i < count($groups_id); $i++) {
                    $datagrp = GroupContacts::create([
                        'user_id' => Auth::id(),
                        'contacts_id' => $data->id,
                        'groups_id' => $groups_id[$i],
                    ]);
                    $datagrp->save();
                }
            } else {
                $datagrp = GroupContacts::create([
                    'user_id' => Auth::id(),
                    'name' => $request->input('name'),
                    'contacts_id' => $data->id,
                    'groups_id' => $groups_id[0],
                ]);
                $datagrp->save();
            }
        } else {
            $datagrp = GroupContacts::create([
                'user_id' => Auth::id(),
                'contacts_id' => $data->id,
                'groups_id' => '',
            ]);
            $datagrp->save();
        }
        Alert::success('Data Berhasil Disimpan');
        return redirect('/address');
    }

    public function updatekontak(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required',
        ]);
        $data = Contacts::find($id);
        $data->name = $request->get('name');
        $data->phone_number = $request->get('phone_number');
        $data->save();
        $groups_id = $request->get('groups_id');
        if (isset($groups_id)) {
            if (is_array($groups_id)) {
                for ($i = 0; $i < count($groups_id); $i++) {
                    $datagrp = GroupContacts::create([
                        'user_id' => Auth::id(),
                        'contacts_id' => $id,
                        'groups_id' => $groups_id[$i],
                    ]);
                    $datagrp->save();
                }
            } else {
                $datagrp = GroupContacts::create([
                    'user_id' => Auth::id(),
                    'name' => $request->input('name'),
                    'contacts_id' => $id,
                    'groups_id' => $groups_id[0],
                ]);
                $datagrp->save();
            }
        }
        Alert::success('Data Berhasil Di Update');
        return redirect('/address');
    }

    public function destroykontak(Request $request)
    {
        $id = $request->input('cid');
        $contact = Contacts::findOrFail($id);
        $contact->delete();
        $group = GroupContacts::where('contacts_id', $id);
        $group->delete();
        Alert::success('Data Berhasil Di Delete');
        return redirect('/address');
    }

    public function destroygroup(Request $request)
    {
        try {
            $ids = $request->cid;
            Groups::whereIn('id', explode(",", $ids))->delete();
            GroupContacts::whereIn('groups_id', explode(",", $ids))->delete();
            Alert::success('Data Berhasil Di Delete');
            return redirect('/address');
        } catch (Exception $exc) {
            $data = Groups::find($ids);
            $data->delete();
            GroupContacts::where('groups_id', $id)->delete();
            Alert::success('Data Berhasil Di Delete');
            return redirect('/address');
        }
    }

    public function fill_select_group()
    {
        $result = DB::table('groups')->select('id', 'name as disp')->get();
        echo json_encode($result);
    }

    public function exportkontak()
    {
        return Excel::download(new \App\Exports\ContactsExport, 'kontak.xlsx');
    }

    public function importkontak()
    {
        Excel::import(new \App\Imports\ContactsImport, request()->file('file'));
        return back();
    }

    public function readFile()
    {
        Excel::import(new \App\Imports\TempImport, request()->file('file'));
    }

    public function json()
    {
        return Datatables::of(Temp::all())->make();
    }

    public function exportgroup()
    {
        return Excel::download(new \App\Exports\GroupsExport, 'group.xlsx');
    }

    public function importgroup()
    {
        Excel::import(new \App\Imports\GroupsImport, request()->file('file'));
        return back();
    }

}
