<?php

namespace App\Http\Controllers;

use App\MaskingManagements;
use App\TypeMaskings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;

class MaskingManagementsController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'type' => TypeMaskings::all(),
            'users' => User::find(Auth::id()),
            'masking' => MaskingManagements::where('user_id', '=', Auth::id())->get(),
        );
        
        return view('pages.masking.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = TypeMaskings::all();
        return view('pages.masking.create', ['rowdata' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'file' => 'required',
            'file.*' => 'doc|mimes:doc,docx',
        ]);
        $id = Auth::id();
        $image = User::find($id);
        $filename = '';
        if ($request->hasFile('file')) {
            $ext = $request->file('file')->getClientOriginalExtension();
            $filename .= 'DocPengajuan_' . $id . '.' . $ext;
            $request->file('file')->move(public_path() . '/masking/', $filename);
        } else {

        }
        $data = MaskingManagements::create([
            'user_id' => Auth::id(),
            'type_id' => $request->input('type_id'),
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'file' => $filename,
            'status' => "Pengajuan",
        ]);
        $data->save();
        Alert::success('Data Berhasil Disimpan');
        return redirect('/maskingmanagement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = array('rowdata' => MaskingManagements::find($id));
        return view('pages.masking.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array('rowdata' => MaskingManagements::find($id));
        return view('pages.masking.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = $request->cid;
        $data = Templates::find($ids);
        $data->delete();
        Alert::success('Data Berhasil Dihapus');
        return redirect('/maskingmanagements');

    }

    public function download()
    {
        $pathToFile = public_path() . '/doc/masking/sample.doc';
        return response()->download($pathToFile);
    }

}
