<?php

namespace App\Http\Controllers;

use App\Templates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;

class TemplatesController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Templates::all();
        session()->put('success','Data Berhasil DiUpdate.');
        return view('pages.template.index', ['rowdata' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'content' => 'required',
        ]);
        $data = Templates::create([
            'user_id' => Auth::id(),
            'name' => $request->input('name'),
            'content' => $request->input('content'),
            'description' => $request->input('description'),
        ]);
        $data->save();
        Alert::success('Data Berhasil Disimpan');
        return redirect('/template');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Templates::find($id);
        return view('pages.template.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'content' => 'required',
        ]);
        $data = Templates::find($id);
        $data->name = $request->get('name');
        $data->content = $request->get('content');
        $data->description = $request->get('description');
        $data->user_id = Auth::id();
        $data->save();
        Alert::success('Data Berhasil Di Update');
        return redirect('/template');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $ids = $request->cid;
            Templates::whereIn('id', explode(",", $ids))->delete();
            Alert::success('Data Berhasil Di Delete');
            return redirect('/template');
        } catch (Exception $exc) {
            $data = Templates::find($ids);
            $data->delete();
            Alert::success('Data Berhasil Di Delete');
            return redirect('/template');
        }
    }

}
