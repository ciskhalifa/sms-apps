<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Alert;

class AccountsController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        $id = Auth::id();
        $rowdata = User::find($id);
        return view('pages.account.index', ['rowdata' => $rowdata]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:800',
        ]);
        $id = Auth::id();
        $image = User::find($id);
        if ($request->hasFile('image')) {
            $ext = $request->file('image')->getClientOriginalExtension();
            $filename = $id . '.' . $ext;
            $request->file('image')->move(public_path() . '/uploads/', $filename);
            $image->image = $filename;
            $image->save();
            Alert::success('Account Updated', 'Requested Complete');
            return redirect('account');
        } else {

        }
    }

    public function updatedata(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
        ]);
        $id = Auth::id();
        $data = User::find($id);
        $data->full_name = $request->get('full_name');
        $data->email = $request->get('email');
        $data->phone_number = $request->get('phone_number');
        $data->save();
        Alert::success('Account Updated', 'Requested Complete');
        return redirect('account');
    }

    public function changepassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);
        $data = $request->all();
        $current_password = Auth::User()->password;
        if (Hash::check($data['old_password'], $current_password)) {
            $user_id = Auth::User()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($data['new_password']);
            $obj_user->save();
            Alert::success('Account Updated', 'Requested Complete');
            return redirect('account');
        } else {
            $error = array('current-password' => 'Please enter correct current password');
            return response()->json(array('error' => $error), 400);
        }
    }

}
