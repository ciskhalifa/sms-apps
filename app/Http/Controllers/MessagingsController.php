<?php

namespace App\Http\Controllers;

use Alert;
use App\Contacts;
use App\Core\CoreSMS;
use App\FileToSMS;
use App\FileToSMSDetail;
use App\Groups;
use App\Jobs\QueueSMS;
use App\MaskingManagements;
use App\Messagings;
use App\SmsCampaignDetail;
use App\Templates;
use App\Temp_SMS;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MessagingsController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        return view('pages.messaging.inbox');
    }

    public function buatKode($type)
    {
        $kodecampaign = '';
        if ($type == 'quick' || $type == 'professional') {
            $nourut = Messagings::where('campaign_type', '=', $type)->max('id');
            if ($nourut == '') {
                $no_urut = 0;
                $kodecampaign = sprintf('CAMP-' . "%03s", abs($nourut + 1)) . '-' . date('Y');
            } else {
                $no = 1;
                $kodecampaign = sprintf('CAMP-' . "%03s", abs($nourut + 1)) . '-' . date('Y');
            }
        } else {
            $nourut = FileToSMS::max('id');
            if ($nourut == null) {
                $no_urut = 0;
                $kodecampaign = sprintf('CAMPFILE-' . "%03s", abs($nourut + 1)) . '-' . date('Y');
            } else {
                $no = 1;
                $kodecampaign = sprintf('CAMPFILE-' . "%03s", abs($nourut + 1)) . '-' . date('Y');
            }
        }
        return $kodecampaign; 
    }

    public function quicksms()
    {
        $list = array(
            //'campaign' => Campaign::all(),
            'masking' => MaskingManagements::where("user_id", '=', Auth::id())->get(),
            'template' => Templates::where("user_id", '=', Auth::id())->get(),
            'kodecampaign' => $this->buatKode('quick'),
        );

        return view('pages.messaging.quicksms')->with($list);
    }

    public function prosms()
    {
        $list = array(
            'group' => Groups::where('user_id', '=', Auth::id())->get(),
            'contact' => Contacts::where('user_id', '=', Auth::id())->get(),
            'masking' => MaskingManagements::where("user_id", '=', Auth::id())->get(),
            'template' => Templates::where("user_id", '=', Auth::id())->get(),
            'kodecampaign' => $this->buatKode('professional'),
        );

        return view('pages.messaging.professionalsms')->with($list);
    }

    public function filetosms()
    {
        $list = array(
            'filetosms' => FileToSMS::where("user_id", '=', Auth::id())->get()
        );
        return view('pages.messaging.filetosms')->with($list);
    }

    public function create()
    {
        $list = array(
            'masking' => MaskingManagements::where("user_id", '=', Auth::id())->get(),
            'kodecampaign' => $this->buatKode('filetosms'),
        );
        return view('pages.messaging.createfiletosms')->with($list);
    }

    public function detail($id)
    {
        $data = array(
            'masking' => MaskingManagements::where("user_id", '=', Auth::id())->get(),
            'filetosms' => FileToSMSDetail::where("filetosms_id", "=", $id)->get(),
        );
        return view('pages.messaging.detail')->with($data);
    }

    public function destroy(Request $request)
    {
        $id = $request->input('cid');
        $filetosms = FileToSMS::findOrFail($id);
        $filetosms->delete();
        $detail = FileToSMSDetail::where('filetosms_id', $id);
        $detail->delete();
        Alert::success('Data Berhasil Di Delete');
        return redirect('/filetosms');
    }
    public function download()
    {
        $pathToFile = public_path() . '/sample/filetosms.xlsx';
        return response()->download($pathToFile);
    }

    public function checkCredit()
    {
        $data = MaskingManagements::where('user_id', '=', $_GET['username'])->get();
        $call = new CoreSMS();
        $req = $call->getCreditInfo($data[0]->username, $data[0]->password);
        $a = array('value' => $req->value, 'expiredDate' => date('d M Y', strtotime($req->expiredDate)));
        echo json_encode($a);

    }

    public function readFile()
    {
        Excel::import(new \App\Imports\TempSMSImport, request()->file('filetosms'));
    }

    public function json()
    {
        return Datatables::of(Temp_SMS::all())->make();
    }

    public function storefilesms(Request $request)
    {

        $masking_id = $request->masking;
        $dataTemp = Temp_SMS::all();
        $dataCampaign = FileToSMS::create([
            'user_id' => Auth::id(),
            'campaign_name' => $request->campaign_name,
            'status' => null,
        ]);
        $dataCampaign->save();
        foreach ($dataTemp as $row) {
            $data = Contacts::create([
                'user_id' => Auth::id(),
                'name' => $row->name,
                'phone_number' => $row->phone_number,
            ]);
            $data->save();
            $dataDetail = FileToSMSDetail::create([
                'user_id' => Auth::id(),
                'filetosms_id' => $dataCampaign->id,
                'contacts_id' => $data->id,
                'maskings_id' => $masking_id,
                'message' => $row->pesan,
                'phone_number' => $data->phone_number,
                'description' => '',
                'scheduler_date' => $row->tgl_kirim,
                'is_sent' => true,
                'status' => null,
            ]);
            $dataDetail->save();
            dispatch(new QueueSMS($dataCampaign->id, $request->username, $request->password, $row->pesan, $row->phone_number, "filetosms"));
        }
        DB::table('temp_sms')->truncate();
        Alert::success('All Message Sent', 'Requested Complete');
        return redirect('/filetosms');

    }

    public function storepro(Request $request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $grp = $request->groups_id;
        $cnt = $request->contacts_id;
        $receiver = $request->receiver[0];
        $scheduler = $request->scheduler_date . ' ' . $request->time_kita;
        $d = DateTime::createFromFormat('m/d/Y', $request->scheduler_date)->format('Y-m-d');
        $dataCampaign = Messagings::create([
            'user_id' => Auth::id(),
            'campaign_type' => 'professional',
            'name' => $request->name,
            'scheduler_status' => 'enable',
            'scheduler_date' => $d,
            'status' => null,
        ]);
        $dataCampaign->save();

        if (!empty($receiver)) {
            $b = explode(',', $receiver);
            for ($y = 0; $y < count($b); $y++) {
                $dataDetail = SmsCampaignDetail::create([
                    'user_id' => Auth::id(),
                    'campaign_id' => $dataCampaign->id,
                    'groups_id' => null,
                    'contacts_id' => null,
                    'templates_id' => $request->template_id,
                    'maskings_id' => $request->masking_id,
                    'message' => $request->messages,
                    'phone_number' => $b[$y],
                    'description' => '',
                    'is_sent' => true,
                    'status' => null,
                ]);
                $dataDetail->save();
                dispatch(new QueueSMS($dataCampaign->id, $request->username, $request->password, $request->messages, $b[$y], "professional"));
            }
        } else {
            if (isset($grp)) {
                for ($i = 0; $i < count($grp); $i++) {
                    $q = Groups::where("id", "=", "$grp[$i]")->get();
                    foreach ($q[0]->contact as $row) {
                        try {
                            $dataDetail = SmsCampaignDetail::create([
                                'user_id' => Auth::id(),
                                'campaign_id' => $dataCampaign->id,
                                'groups_id' => $q[0]->id,
                                'contacts_id' => $row->id,
                                'templates_id' => $request->template_id,
                                'maskings_id' => $request->masking_id,
                                'status' => null,
                            ]);
                        } catch (\Illuminate\Database\QueryException $e) {
                            $errorCode = $e->errorInfo[1];
                        }
                        dispatch(new QueueSMS($dataCampaign->id, $request->username, $request->password, $request->messages, $row->phone_number, "professional"));
                    }
                }
            }
            if (isset($cnt)) {
                for ($a = 0; $a < count($cnt); $a++) {
                    $q = Contacts::where("id", "=", "$cnt[$a]")->get();
                    foreach ($q[0]->group as $row) {
                        try {
                            $dataDetail = SmsCampaignDetail::create([
                                'user_id' => Auth::id(),
                                'campaign_id' => $dataCampaign->id,
                                'groups_id' => $row->id,
                                'contacts_id' => $q[0]->id,
                                'templates_id' => $request->template_id,
                                'maskings_id' => $request->masking_id,
                                'message' => $request->messages,
                                'phone_number' => $row->phone_number,
                                'description' => '',
                                'is_sent' => true,
                                'status' => null,
                            ]);
                        } catch (\Illuminate\Database\QueryException $e) {
                            $errorCode = $e->errorInfo[1];
                        }
                        dispatch(new QueueSMS($dataCampaign->id, $request->username, $request->password, $request->messages, $row->phone_number, "professional"));
                    }
                }
            }
        }
        Alert::success('All Message Sent', 'Requested Complete');
        return redirect('/prosms');
    }

    public function store(Request $request)
    {
        $a = $request->receiver[0];
        $b = explode(',', $a);
        $dataCampaign = Messagings::create([
            'user_id' => Auth::id(),
            'campaign_type' => 'quick',
            'name' => $request->name,
            'scheduler_status' => 'disable',
            'scheduler_date' => null,
            'status' => null,
        ]);
        $dataCampaign->save();

        for ($i = 0; $i < count($b); $i++) {
            $dataDetail = SmsCampaignDetail::create([
                'user_id' => Auth::id(),
                'campaign_id' => $dataCampaign->id,
                'groups_id' => null,
                'contacts_id' => null,
                'templates_id' => $request->template_id,
                'maskings_id' => $request->masking_id,
                'message' => $request->messages,
                'phone_number' => $b[$i],
                'description' => '',
                'is_sent' => true,
                'status' => null,
            ]);
            $dataDetail->save();
            dispatch(new QueueSMS($dataCampaign->id, $request->username, $request->password, $request->messages, $b[$i], "quick"));
        }
        Alert::success('All Message Sent', 'Requested Complete');
        return redirect('/quicksms');
    }
}
