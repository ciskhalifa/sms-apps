<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model {

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'description'
    ];

    public function contact() {
        return $this->belongsToMany(Contacts::class, 'contacts_groups');
    }

}
