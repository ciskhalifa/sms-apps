<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'phone_number',
    ];

    public function group()
    {
        return $this->belongsToMany(Groups::class, 'contacts_groups');
    }

    public function contact()
    {
        return $this->belongsToMany(FileToSMSDetail::class, 'contacts_id');
    }

}
