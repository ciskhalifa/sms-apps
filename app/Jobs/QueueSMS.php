<?php

namespace App\Jobs;

use App\Core\CoreSMS;
use App\Messagings;
use App\SmsCampaignDetail;
use App\FileToSMS;
use App\FileToSMSDetail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class QueueSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $username, $password, $text, $receiver, $id_campaign, $data, $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id_campaign, $username, $password, $text, $receiver, $type)
    {
        $this->id_campaign = $id_campaign;
        $this->username = $username;
        $this->password = $password;
        $this->text = $text;
        $this->receiver = $receiver;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->type == "quick" || $this->type == "professional") {
            $dataCampaign = Messagings::find($this->id_campaign);
            $call = new CoreSMS();
            $req = $call->sendSMS($this->username, $this->password, $this->text, $this->receiver);
            $dataCampaign->status = $req['statusCode'];
            $dataCampaign->save();
            SmsCampaignDetail::where('campaign_id', '=', $this->id_campaign)->update(
                ['status' => $req['statusCode'],
                    'description' => $req['message'],
                    'is_sent' => $req['status'],
                ]);
        }else{
            $dataCampaign = FileToSMS::find($this->id_campaign);
            $call = new CoreSMS();
            $req = $call->sendSMS($this->username, $this->password, $this->text, $this->receiver);
            $dataCampaign->status = $req['statusCode'];
            $dataCampaign->save();
            FileToSMSDetail::where('filetosms_id', '=', $this->id_campaign)->update(
                    ['status' => $req['statusCode'],
                    'description' => $req['message'],
                    'is_sent' => $req['status'],
                ]);
        }
    }
}
