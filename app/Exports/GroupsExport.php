<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Auth;

class GroupsExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('groups')->select('id', 'name', 'description', 'created_at', 'updated_at')->where('user_id', Auth::id())->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Description',
            'Created at',
            'Updated at',
        ];
    }
}
