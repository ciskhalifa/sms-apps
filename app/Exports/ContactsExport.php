<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactsExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('contacts')->select('id', 'name', 'phone_number', 'created_at', 'updated_at')->where('user_id', Auth::id())->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Phone Number',
            'Created at',
            'Updated at',
        ];
    }
}
