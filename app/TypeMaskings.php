<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeMaskings extends Model
{
    protected $table = 'type';
    protected $fillable = [
        'id',
        'type_name',
    ];

    public function masking()
    {
        return $this->hasMany(MaskingManagements::class, 'id');
    }
}
