<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupContacts extends Model {

    protected $table = 'contacts_groups';
    protected $fillable = [
        'id',
        'user_id',
        'groups_id',
        'contacts_id'
    ];

   

}
