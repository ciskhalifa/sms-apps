<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'templates';
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'content',
        'description'
    ];
}
