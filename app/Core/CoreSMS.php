<?php

namespace App\Core;

use Alert;
use GuzzleHttp\Client as HttpClient;

class CoreSMS
{
    private $httpClient;
    private $responseCodes = [
        0 => "Request was successful",
        -1 => "Error in processing the request",
        -2 => "Not enough credits on a specific account",
        -3 => "Targeted network is not covered on specific account",
        -5 => "Username or password is invalid",
        -6 => "Destination address is missing in the request",
        -7 => "Balance has expired",
        -11 => "Number is not recognized by NusaSMS platform",
        -12 => "Message is missing in the request",
        -13 => "Number is not recognized by NusaSMS platform",
        -22 => "Incorrect XML format, caused by syntax error",
        -23 => "General error, reasons may vary",
        -26 => "General API error, reasons may vary",
        -27 => "Invalid scheduling parameter",
        -28 => "Invalid PushURL in the request",
        -30 => "Invalid APPID in the request",
        -33 => "Duplicated MessageID in the request",
        -34 => "Sender name is not allowed",
        -99 => "Error in processing request, reasons may vary",
    ];

    public function __construct()
    {
        $this->httpClient = new HttpClient(
            ['base_uri' => 'http://api.smscenter.net/api/']
        );
    }
    public function getCreditInfo($username, $password)
    {
        try {
            $result = $this->httpClient->request("GET", "command", [
                'query' => [
                    'user' => $username,
                    'password' => $password,
                    'cmd' => 'CREDITS',
                    'output' => 'json',
                ],
            ]);
            if ($result->getStatusCode() != 200) {
                Alert::error('Error Getting Data');
                return null;
            }

            $body = $result->getBody();
            $content = $body->getContents();
            return json_decode($content);
        } catch (\Exception $e) {
            throw new \Exception("Error Getting Credit Info:" . $e->getMessage());
        }
    }
    public function sendSMS($username, $password, $text, $receiver)
    {
        try {
            // return json_decode('{"results":[{"status":"0","messageid":"","destination":"+6285659937810"}]}');
            $data = array(
                'user' => $username,
                'password' => $password,
                'SMSText' => $text,
                'GSM' => $receiver,
                'output' => 'json',
            );

            if (!($creditInfo = $this->getCreditInfo($username, $password))) {
                return false;
            }

            if ($creditInfo->value <= 0) {
                Alert::error('Not enough credits on a specific account');
                return false;
            }

            $result = $this->httpClient->post('v3/sendsms/plain', ['form_params' => $data]);
            if ($result->getStatusCode() != 200) {
                Alert::error('Error Sending SMS');
                return null;
            }

            $body = $result->getBody();
            $content = $body->getContents();

            return $this->ResolveSMSResponse(json_decode($content));

        } catch (\Exception $e) {
            throw new \Exception("Error Sending SMS:" . $e->getMessage());
        }
    }

    private function ResolveSMSResponse($response)
    {
        $responseData = $response->results[0];
        $result = [
            'status' => false,
            'statusCode' => $responseData->status,
            'message' => 'Unrecognized Status From SMS Provider',
        ];

        if ($responseData->status == 0) {
            $result['status'] = true;
            $result['message'] = "Request was sent";
        } else if (array_key_exists($responseData->status, $this->responseCodes)) {
            $result['message'] = $this->responseCodes[$responseData->status];
        } else {
            $result['message'] = "Unrecognized Status From SMS Provider";
        }

        return $result;
    }
}
