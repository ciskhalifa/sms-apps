<?php

namespace App\Imports;

use App\Temp_SMS;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TempSMSImport implements ToModel, WithHeadingRow, SkipsOnError
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $date = DateTime::createFromFormat('d/m/Y', $row['tanggal_kirim']);
        $id = Auth::id();
        return new Temp_SMS([
            'name' => $row['name'],
            'pesan' => $row['pesan'],
            'phone_number' => $row['phone_number'],
            'tgl_kirim' => $date->format('Y-m-d')
        ]);
    }

    public function onError(\Throwable $e)
    {

    }
}
