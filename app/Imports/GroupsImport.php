<?php

namespace App\Imports;

use App\Groups;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GroupsImport implements ToModel, WithHeadingRow {

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row) {
        return new Groups([
            'user_id' => Auth::id(),
            'name' => $row['name'],
            'description' => $row['description'],
        ]);
    }

}
