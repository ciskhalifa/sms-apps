<?php

namespace App\Imports;

use App\Contacts;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;

class ContactsImport implements ToModel, WithHeadingRow, SkipsOnError {

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row) {
        return new Contacts([
            'user_id' => Auth::id(),
            'name' => $row['name'],
            'phone_number' => $row['phone_number'],
        ]);
    }

    public function onError(\Throwable $e) {
        
    }

}
