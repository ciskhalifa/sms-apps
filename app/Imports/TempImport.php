<?php

namespace App\Imports;

use App\Temp;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;

class TempImport implements ToModel, WithHeadingRow, SkipsOnError {

    /**
     * @param Collection $collection
     */
    public function model(array $row) {
        return new Temp([
            'name' => $row['name'],
            'phone_number' => $row['phone_number'],
            'user_id' => Auth::id()
        ]);
    }

    public function onError(\Throwable $e) {
        
    }

}
