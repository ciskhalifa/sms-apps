<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_SMS extends Model
{
    protected $table = 'temp_sms';
    protected $fillable = [
        'id',
        'name',
        'phone_number',
        'pesan',
        'tgl_kirim'
    ];

}
