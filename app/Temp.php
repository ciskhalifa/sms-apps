<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp extends Model {

    protected $table = 'temp';
    protected $fillable = [
        'id',
        'name',
        'phone_number'
    ];

}
