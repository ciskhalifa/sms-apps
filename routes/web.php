<?php

use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('sendQueue', 'MessagingsController@sendQueue');
Route::get('/', function () {
    return view('auth.login');
});
// Route::get('/token', function(){
//     return csrf_token();
// });

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// ROUTE MESSAGING
Route::get('/inbox', function () {
    return view('pages.messaging.inbox');
});

// ROUTE QUICK SMS
Route::post('/queuesms', 'MessagingsController@QueueSMS');
Route::get('/checkcredits', 'MessagingsController@checkCredit')->name('checkCredit');
Route::get('/quicksms', 'MessagingsController@quicksms');
Route::post('/quicksms/store', 'MessagingsController@store')->name('quick.store');
// ROUTE PROFESSIONAL SMS
Route::get('/prosms','MessagingsController@prosms');
Route::post('/prosms/storepro', 'MessagingsController@storepro')->name('pro.store');
// ROUTE FILE TO SMS
Route::get('/filetosms', 'MessagingsController@filetosms')->name('filetosms');
Route::get('/filetosms/create', 'MessagingsController@create')->name('filetosms.create');
Route::post('/filetosms/store', 'MessagingsController@storefilesms')->name('filetosms.store');
Route::post('/filetosms/readFile', 'MessagingsController@readFile')->name('read.file');
Route::get('/filetosms/json', 'MessagingsController@json');
Route::get('/filetosms/download', 'MessagingsController@download')->name('sample.download');
Route::get('/filetosms/{id}/detail', 'MessagingsController@detail')->name('filetosms.detail');
Route::delete('/filetosms/destroy', 'MessagingsController@destroy')->name('filetosms.destroy');

// ROUTE MESSAGING Whatsapp
Route::get('/inboxwa', function () {
    return view('pages.whatsapp.inbox');
});
Route::get('/blastwa', function () {
    return view('pages.whatsapp.blast');
});
Route::get('/filetowa', function () {
    return view('pages.whatsapp.filetowa');
});
Route::get('/deliverywa', function () {
    return view('pages.whatsapp.delverywa');
});

//ROUTE TEMPLATE
Route::get('/template', 'TemplatesController@index')->name('template.index');
Route::get('/template/create', 'TemplatesController@create')->name('template.create');
Route::get('/template/{id}/edit', 'TemplatesController@edit')->name('template.edit');
Route::post('/template', 'TemplatesController@store')->name('template.store');
Route::delete('/template/{id}', 'TemplatesController@destroy')->name('template.destroy');
Route::patch('/template/{id}', 'TemplatesController@update')->name('template.update');

//ROUTE MASKING MANAGEMENT
Route::get('/maskingmanagement', 'MaskingManagementsController@index')->name('masking.index');
Route::get('/maskingmanagement/create', 'MaskingManagementsController@create')->name('masking.create');
Route::get('/maskingmanagement/{id}/edit', 'MaskingManagementsController@edit')->name('masking.edit');
Route::post('/maskingmanagement', 'MaskingManagementsController@store')->name('masking.store');
Route::delete('/maskingmanagement/{id}', 'MaskingManagementsController@destroy')->name('masking.destroy');
Route::patch('/maskingmanagement/{id}', 'MaskingManagementsController@update')->name('masking.update');
Route::get('/maskingmanagement/download', 'MaskingManagementsController@download')->name('masking.download');
Route::get('/maskingmanagement/{id}/detail', 'MaskingManagementsController@show')->name('masking.detail');

//ROUTE ACCOUNT SETTING
Route::get('/account', 'AccountsController@index');
Route::post('/account/update', 'AccountsController@update')->name('account.changeimage');
Route::post('/account/updatedata', 'AccountsController@updatedata')->name('account.updatedata');
Route::post('/account/changepassword', 'AccountsController@changepassword')->name('account.changepassword');

//ROUTE ADDRESS BOOK
Route::get('/address', 'AddressBooksController@index');
Route::get('/address/kontak', 'AddressBooksController@createkontak')->name('create.contact');
Route::get('/address/{id}/editkontak', 'AddressBooksController@editkontak')->name('edit.contact');
Route::get('/address/{id}/detail', 'AddressBooksController@detail')->name('detail.contact');
Route::patch('/address/{id}/updatekontak', 'AddressBooksController@updatekontak')->name('update.contact');
Route::delete('/address/destroykontak', 'AddressBooksController@destroykontak')->name('contact.destroy');

Route::get('/address/group', 'AddressBooksController@creategroup')->name('create.group');
Route::get('/address/{id}/editgroup', 'AddressBooksController@editgroup')->name('edit.group');
Route::patch('/address/{id}/updategroup', 'AddressBooksController@updategroup')->name('update.group');
Route::delete('/address/destroygroup', 'AddressBooksController@destroygroup')->name('group.destroy');

Route::post('/address/kontak', 'AddressBooksController@simpankontak')->name('contact.create');
Route::post('/address/group', 'AddressBooksController@simpangroup')->name('group.create');
Route::post('/address/fill_select_group', 'AddressBooksController@fill_select_group')->name('fill_select_group');

Route::get('/address/exportkontak', 'AddressBooksController@exportkontak')->name('export.contact');
Route::get('/address/exportgroup', 'AddressBooksController@exportgroup')->name('export.group');

Route::post('/address/importkontak', 'AddressBooksController@importkontak')->name('import.contact');
Route::post('/address/importgroup', 'AddressBooksController@importgroup')->name('import.group');

Route::post('/address/readFile', 'AddressBooksController@readFile')->name('read.file');
Route::get('/address/json', 'AddressBooksController@json');
Route::post('/address/grouping', 'AddressBooksController@grouping');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
